import React from 'react';
import {
  View,Text, ScrollView, Image,
  TouchableOpacity,Linking,Animated
} from 'react-native';


class App extends React.Component {
    state={animasyon:new Animated.Value(1)}
    animasyonuBaslat=()=>{

      Animated.timing(this.state.animasyon,{
        toValue:0,
        duration:2000,
      }).start(()=>{
        Animated.timing(this.state.animasyon,{
          toValue:1,
          duration:1000
        }).start()
      })

    }

  render() {
    return (
<View style={{marginTop:100}}>
  <TouchableOpacity onPress={this.animasyonuBaslat}>
    <Animated.View style={{backgroundColor:"red",
    width:100,height:100,
    opacity:this.state.animasyon}}>
      <Text>Animation</Text>
    </Animated.View>
    </TouchableOpacity>
</View>

    );
  }

}
export default App;
