import React, { Component } from 'react';
import { View, Text ,Image,TouchableOpacity,FlatList} from 'react-native';
import Axios from 'axios';
import BASE from './constants';
import FavoriStore from './FavoriStore';

export default class Anasayfa extends Component {
  
  state={data:[]}
  constructor(props) {
    super(props);
    this.getUrunler()
    
  }
getUrunler=async()=>{

        const favoriler = await FavoriStore.getData()    
        Axios.get(BASE,{params:{"target":"listUruns"}}).then(res=>{
                const urunler = res.data
                urunler.forEach((urun)=>{ 
                  if(favoriler.length>0)
                  {
                    favoriler.forEach(id=>{
                        if(urun.id==id && id>0)
                        {
                          FavoriStore.addUrun(urun)
                        }
                    })
                  }                   
                  

                })
                this.setState({data:res.data})
        }).catch(error=>{
                alert("hata oluştu")

        })

}

renderItem=({item,index})=>{
    return (
        <TouchableOpacity
        onPress={()=>this.props.navigation.navigate("Details",{urun:item})}
        
        style={{backgroundColor:"white",margin:5,borderRadius:10,borderColor:"gray"}}>
            <View style={{margin:10}}>
                <Image source={{uri:item.urun_photo}}
                 resizeMode={"contain"} 
                style={{width:140,height:150}}/>
            </View>
            <View style={{margin:10}}>
              <Text style={{width:140}}>{item.urun_adi}</Text>
              <Text>{item.fiyat} {"₺"}</Text>
            
            
            </View>

            
        </TouchableOpacity>
    )}
  render() {     
    return (
      <View style={{alignItems:"center"}}>
          <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          numColumns={2}
          keyExtractor={(item)=>item.id}
          data={this.state.data}
    
          />
        
      </View>
    );
  }
}
