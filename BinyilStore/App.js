import 'react-native-gesture-handler';
import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Login';
import HomeTab from "./Home"
import Register from './Register';
import ForgetPsw from './ForgetPsw';
import Details from "./Details";
import ProfileUpdate from "./ProfileUpdate"
const Stack = createStackNavigator();

export default class App extends Component {

  render() {
    return (
      <NavigationContainer>
      <Stack.Navigator screenOptions={{header:()=>null}}>
        <Stack.Screen name="Login"  component={Login} />
        <Stack.Screen name="Register"  component={Register} />
        <Stack.Screen name="ForgetPsw"  component={ForgetPsw} />
        <Stack.Screen name="HomeTab"  component={HomeTab} />
        <Stack.Screen name="Details" component={Details}/>
        <Stack.Screen name="ProfileUpdate" component={ProfileUpdate}/>
      
      </Stack.Navigator>
    </NavigationContainer>
    )
  }
}
