import React, { Component } from 'react';
import { View, Text, BackHandler,ScrollView, Image,StatusBar } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import {faHeart} from "@fortawesome/free-solid-svg-icons"
import { observer } from 'mobx-react';
import FavoriStore from './FavoriStore';
import Axios from 'axios';
import BASE from './constants';
import UserStore from "./UserStore"

@observer
export default class Details extends Component { 
  
        state={inFavs:false}
        
        onBackPress = async ()=>{
                
                const {urun} = this.props.route.params 
                if(this.state.inFavs)
                {
                    FavoriStore.addUrun(urun)
                }else{
                    FavoriStore.removeUrun(urun)
                }
                FavoriStore.storeData()
                BackHandler.removeEventListener("hardwareBackPress",this.onBackPress)
                this.props.navigation.pop()
        }

        componentDidMount()
        {
            const {urun} = this.props.route.params 
            this.isFavorite(urun)
            BackHandler.addEventListener("hardwareBackPress",this.onBackPress)

        }
        isFavorite=(urun)=>{

                var favories = FavoriStore.urunler
                if(favories.length>0)
                {
                    favories.forEach((ur)=>{
                        if(urun.id==ur.id)
                        {
                            this.setState({inFavs:true})
                        }
                    })
                }

        }

        addToFavorite=()=>{
            this.setState({inFavs:!this.state.inFavs})
        }
        addToBasket=(urun)=>{

                    Axios.get(BASE,{params:{
                        target:"sepeteEkle",
                        urunId:urun.id,
                        fiyat:urun.fiyat,
                        urunAdi:urun.urun_adi,
                        userId:UserStore.userId
                    }}).then(_result=>{
                        const {result}=_result.data
                        if(result=="success")
                        {
                            alert("Ürününüz başarıyla eklendi")
                        }else{
                            alert("Ürün eklenemedi! Tekrar deneyiniz...")
                        }
                    }).catch(error=>{
                            alert("hata oluştu :"+error)
                    })


        }
  render() {
    const {urun} = this.props.route.params 
    return (
      <ScrollView showsVerticalScrollIndicator={false} 
      style={{backgroundColor:"white",margin:10}} >
          <StatusBar hidden={true}/>
          <View style={{flexDirection:"row",flex:1,}}>
         
              <View style={{width:200,}}>
                  <Image source={{uri:urun.urun_photo}} style={{width:120,height:200}}
                   resizeMode={"contain"}/>
                     <View style={{
                         position:"absolute",width:100,height:100,marginLeft:60,marginTop:50,}}>
                    <TouchableOpacity onPress={this.addToFavorite}>
                      <FontAwesomeIcon icon={faHeart} 
                      size={50} color={this.state.inFavs?"red":"gray"}/>
                    </TouchableOpacity>
                  </View>
              </View>            
          
          <View style={{marginTop:30}}>
              <Text style={{width:200,}}>{urun.urun_adi}</Text>
              <Text>{urun.fiyat} {"₺"}</Text>
              <TouchableOpacity 
                    onPress={()=>this.addToBasket(urun)}
              
              style={{padding:10,justifyContent:"center",alignItems:"center"}}>
                  <Text style={{width:120,
                  textAlign:"center",
                  textAlignVertical:"center",
                  backgroundColor:"orange",
                  borderColor:"orange",
                  alignItems:"center",
                  borderWidth:10,
                  borderRadius:20,                  
                  }}>
                      {"Sepete Ekle"}
                  </Text>

              </TouchableOpacity>

          </View>

          </View>
          <View style={{marginTop:20,}}>
              <Text>{urun.urun_aciklamasi}</Text>
          </View>
        
      </ScrollView>
    );
  }
}
