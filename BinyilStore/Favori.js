import React, { Component } from 'react';
import { View, Text,FlatList,TouchableOpacity,Image } from 'react-native';
import { observer } from 'mobx-react';
import FavoriStore from "./FavoriStore"

@observer
export default class Favori extends Component {

  
  state={data:[]}
  
  componentDidMount()
  {

    this.props.navigation.addListener("focus",()=>{
      this.getUrunler()
    })
   
  }
 
  getUrunler=()=>{

        const favoriler = FavoriStore.getUrunler()
        this.setState({data:favoriler})

  }

renderItem=({item,index})=>{
    return (
        <TouchableOpacity
        onPress={()=>this.props.navigation.navigate("Details",{urun:item})}
        
        style={{backgroundColor:"white",margin:5,borderRadius:10,borderColor:"gray"}}>
            <View style={{margin:10}}>
                <Image source={{uri:item.urun_photo}}
                 resizeMode={"contain"} 
                style={{width:190,height:200}}/>
            </View>
            <View style={{margin:10}}>
              <Text style={{width:140}}>{item.urun_adi}</Text>
              <Text>{item.fiyat} {"₺"}</Text>
            
            
            </View>

            
        </TouchableOpacity>
    )}
  render() {     
    return (
      <View style={{alignItems:"center"}}>
          <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item)=>item.id}
          data={this.state.data}
    
          />
        
      </View>
    );
  }


}
