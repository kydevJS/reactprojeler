import {observable,action} from "mobx"
import AsyncStorage from "@react-native-community/async-storage"
class FavoriStore{

    FAVORI="@favoriler"
    @observable urunler=[]
    @action
    addUrun=(urun)=>{

        var isIn=false;
        this.urunler.forEach((ur)=>{
            if (ur.id==urun.id)
            {
                isIn=true;
            }
        })
        if(isIn==false)
            this.urunler.push(urun)
    }
    @action 
    getUrunler=()=>this.urunler;

    @action 
    removeUrun=(urun)=>{

            this.urunler=this.urunler.filter((ur)=>urun.id!=ur.id)

    }
    @action 
    storeData=async ()=>{
        
        var str = ""
        this.urunler.forEach((urun)=>{
            str +=urun.id+","
        })
        try{

            if(str!=""){
                
                await AsyncStorage.setItem(this.FAVORI,str)
            }
        }catch(e){

        }
    }

    @action
    getData=async()=>{
        try{
            
           const datas = await AsyncStorage.getItem(this.FAVORI);         
            if(datas!=null)
            {
                const lst = datas.split(",");
                return lst             
            }else{
                return []
            }
           

        }catch(e){
           
                return []
        }
    }




}
export default new FavoriStore()