import React, { Component } from 'react'
import { Text, View, TextInput, KeyboardAvoidingView,StyleSheet,
  ActivityIndicator, 
  Alert} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';
import BASE from './constants';

export default class ForgetPsw extends Component {
    state={
      loading:false,
      email:"",
      error:""
    }

    getUserPsw = async()=>{
            
        if(this.state.email.length<3) return;
        this.setState({loading:true}) 

      axios.get(BASE,{params:{"target":"forgetPassword","email":this.state.email}}).then(res=>{         
         
         //alert(JSON.stringify(res.data))          
          this.gotoBack();
          this.setState({loading:false})
        
      }).catch((error)=>{
        alert("Hata oluştu "+error)
        this.setState({loading:false})
      });

    }
    
    gotoBack=()=>{
        this.props.navigation.pop();
    }

//desen pattern  @ olacak . olacak noktadan sonra en az 2 haneli karakter
  validateEmail=(text)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text))
    {
      this.setState({email:text,error:""})
     
    }else{
      this.setState({error:"email formatını doğru giriniz..."})
    }
  }








  render() {
    if(this.state.loading)
      return (<View style={{alignItems:"center",justifyContent:"center",flex:1}}>
        <ActivityIndicator color={"red"} size={"large"}/>
        </View>)
    else
       return (
      <KeyboardAvoidingView style={{alignItems:"center",
      justifyContent:"center",flex:1}}>

          <Text>Binyıl Store Şifre hatırlatma</Text>
          <View>
            <Text style={{color:"red"}}>{this.state.error}</Text>
          </View>
          
          <View>
            <TextInput inlineImageLeft="email" style={styles.input} 
            onChangeText={(text)=>this.validateEmail(text)}
            placeholder="Email giriniz.." />
          </View>
        
         
          <View>
            <TouchableOpacity style={styles.button} onPress={()=>this.getUserPsw()}>
              <Text style={{textAlign:"center",color:'white'}}>{"Kayıt ol"}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.newRecord}>          
          <TouchableOpacity onPress={this.gotoBack} style={{marginLeft:10,backgroundColor:'blue',borderRadius:5}}>
            <Text style={{color:'white',padding:5}}>Geri</Text>
          </TouchableOpacity>
        </View>
      
         
      </KeyboardAvoidingView>
    )
  }
}
const styles=StyleSheet.create({
    input:{
      borderBottomWidth:1,
      borderBottomColor:"black",
      textAlign:"center",
      marginTop:10,
      width:300
    },
    button:{
      marginTop:20,
      borderRadius:15,
      borderWidth:1,
      paddingHorizontal:20,
      paddingVertical:5,
      width:300,
      marginBottom:15, 
      backgroundColor:'blue'
     
    },
    newRecord:{
      flexDirection:'row', 
      marginTop:10,
      height:30,
      justifyContent:'center', 
      alignItems:'center',
      marginBottom:10

     
    },
    ForgetPassword:{
      marginTop:5,
    }
});