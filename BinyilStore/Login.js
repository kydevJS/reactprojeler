import React, { Component } from 'react'
import { Text, View, TextInput, KeyboardAvoidingView,StyleSheet,
  ActivityIndicator, 
  Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';
import BASE from './constants';

import UserStore from "./UserStore"
import { observer } from 'mobx-react';

@observer
export default class Login extends Component {
    state={
      loading:true,
      email:"",
      uid:"",
      password:"",
      error:""
    }

    gotoPage=(pagename)=>{
        this.props.navigation.navigate(pagename)
    }
    checkUser = async(email,password,firstLoad)=>{

      this.setState({loading:true})
      axios.get(BASE,{
        params:{"email":email,
        "password":password,
        "target":"login"}

      }).then(res=>{         
         
          const {username,id}=res.data;
          UserStore.setUser(username,password,id,email)
          if(firstLoad==false)
              this.storeUser(email,password)           
          this.props.navigation.replace("HomeTab")
        
      }).catch((error)=>{
        alert("Hata oluştu "+error)
        this.setState({loading:false})
      });

    }
    getUser = async ()=>{
      try {
        const email = await AsyncStorage.getItem('@email')
        const password =await AsyncStorage.getItem('@password')
        if(email.length>2 && password.length>2)
        {
            this.checkUser(email,password,true)

        }
        

      } catch (e) {
        this.setState({loading:false})
      }
    }
    componentDidMount()
    {
      
     this.getUser();
    }
    storeUser = async(email,password)=>{
      try {
          await AsyncStorage.setItem('@email', email)
         await AsyncStorage.setItem('@password', password)
      } catch (e) {
        // saving error
      }

    }
  userLogin=()=>{

    if(this.state.password.length<1 || this.state.email.length<2) return;

    this.checkUser(this.state.email,this.state.password,false)
    


  }
//desen pattern  @ olacak . olacak noktadan sonra en az 2 haneli karakter
  validateEmail=(text)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text))
    {
      this.setState({email:text,error:""})
     
    }else{
      this.setState({error:"email formatını doğru giriniz..."})
    }
  }
  validatePsw=(text)=>{
    if(text.length>2){
      this.setState({password:text,error:""})
     
    }else{
      this.setState({error:"şifreniz en az 3 haneli olmalıdır."})
    }
  }

getData = async (ekstra) => {
  try {
    const value = await AsyncStorage.getItem('anahtar')
    if(value !== null) {
      this.key=value+ekstra
    }
  } catch(e) {
    this.key=""
  }
}

  writeData = async (value) => {
    try {
      await AsyncStorage.setItem('anahtar', value)
    } catch (e) {
      // saving error
    }
  }

  render() {
    if(this.state.loading)
      return (<View style={{alignItems:"center",justifyContent:"center",flex:1}}>
        <ActivityIndicator color={"red"} size={"large"}/>
        </View>)
    else
       return (
      <KeyboardAvoidingView style={{alignItems:"center",
      justifyContent:"center",flex:1}}>

          <Text>Binyıl Store Login</Text>
          <View>
            <Text style={{color:"red"}}>{this.state.error}</Text>
          </View>
          <View>
            <TextInput inlineImageLeft="email" style={styles.input} 
            onChangeText={(text)=>this.validateEmail(text)}
            placeholder="Email giriniz.." />
          </View>
          <View>
            <TextInput  inlineImageLeft="psw" style={styles.input}
            onChangeText={(text)=>this.validatePsw(text)}
          
            secureTextEntry={true}
            placeholder="Parola giriniz.." />
          </View>
          <View>
            <TouchableOpacity style={styles.button} onPress={()=>this.userLogin()}>
              <Text style={{textAlign:"center",color:'white'}}>{"Giriş"}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.newRecord}>
          <Text style={{justifyContent:'center'}}>Kaydın yok mu?</Text>
            <TouchableOpacity onPress={()=>this.gotoPage("Register")} style={{marginLeft:10,backgroundColor:'blue',borderRadius:5}}>
              <Text style={{color:'white',padding:5}}>Kaydol</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={()=>this.gotoPage("ForgetPsw")} style={styles.ForgetPassword}>
          <Text>Şifremi Unuttum</Text>
          </TouchableOpacity>
      </KeyboardAvoidingView>
    )
  }
}
const styles=StyleSheet.create({
    input:{
      borderBottomWidth:1,
      borderBottomColor:"black",
      textAlign:"center",
      marginTop:10,
      width:300
    },
    button:{
      marginTop:20,
      borderRadius:15,
      borderWidth:1,
      paddingHorizontal:20,
      paddingVertical:5,
      width:300,
      marginBottom:15, 
      backgroundColor:'blue'
     
    },
    newRecord:{
      flexDirection:'row', 
      marginTop:10,
      height:30,
      justifyContent:'center', 
      alignItems:'center',
      marginBottom:10

     
    },
    ForgetPassword:{
      marginTop:5,
    }
});