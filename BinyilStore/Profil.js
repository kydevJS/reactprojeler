import React, { Component } from 'react';
import { View, Text,StyleSheet } from 'react-native';
import { observer } from 'mobx-react';
import UserStore from './UserStore';
import { TouchableOpacity } from 'react-native-gesture-handler';
import AsyncStorage from "@react-native-community/async-storage";
@observer
export default class Profil extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  deleteUser = async()=>{
      try {
          await AsyncStorage.setItem('@email', '')
         await AsyncStorage.setItem('@password', '')
      } catch (e) {
        // saving error
      }

  }


  exitApp=()=>{

    this.deleteUser()
    UserStore.resetUser()
    this.props.navigation.replace("Login")        

  }
  render() {
    const pswLength = UserStore.password.length
    var star =""
    for(var i=0;i<pswLength;i++)
    {
      star +="*"
    }

    return (
      <View style={{alignItems:"center",justifyContent:"center",flex:1,}}>
        <Text style={{fontSize:20,fontWeight:"bold",color:"blue"}}>{"Binyıl Store Kullanıcı Profili"}</Text>
        <View>
          <Text style={styles.mstil}>{UserStore.username}</Text>
        </View>
        <View>
          <Text style={styles.mstil}>{UserStore.email}</Text>
        </View>
        <View style={styles.mstil}>
          <Text>{star}</Text>
        </View>
        <View>
          <TouchableOpacity onPress={this.exitApp} style={styles.mstil}>
            <Text style={{color:"red"}}>{"Çıkış Yap"}</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate("ProfileUpdate")} style={styles.mstil}>
            <Text>{"Bilgileri güncelle"}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles=StyleSheet.create({
  mstil:{
    width:200,
    marginTop:20,
    fontSize:16,
    color:"red",fontWeight:"bold",
    borderColor:"gray",
    borderRadius:10,
    borderBottomWidth:1,
    paddingHorizontal:30,
    paddingVertical:10,
  }
})