import React, { Component } from 'react'
import { Text, View, TextInput, KeyboardAvoidingView,StyleSheet,
  ActivityIndicator, 
  Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';
import BASE from './constants';
import UserStore from "./UserStore"
import { observer } from 'mobx-react';

@observer
export default class ProfileUpdate extends Component {
    state={
      loading:false,
      email:"",
      uid:"",
      password:"",
      error:"",
      username:""
    }
  

    storeUser = async(email,password)=>{
      try {
          await AsyncStorage.setItem('@email', email)
         await AsyncStorage.setItem('@password', password)
      } catch (e) {
        // saving error
      }

    }

  userLogin=()=>{

        var username=this.state.username
        var password=this.state.password
        var email=this.state.email

        if(this.state.password.length<2)
            password=UserStore.password
        if(this.state.email.length<2)
            email=UserStore.email
        if(this.state.username.length<2)
            username=UserStore.username

        const params = new URLSearchParams();
        params.append("email",email)
        params.append("username",username)
        params.append("userId",UserStore.userId)
        params.append("password",password)
        params.append("target","updateUser")
        this.setState({loading:true})
        axios.post(BASE,params).then(res=>{
            const {result}=res.data
            if(result=="success"){
                    UserStore.username=username
                    UserStore.password=password
                    UserStore.email=email
                    this.storeUser(email,password)
                    this.props.navigation.pop()

            }
            this.setState({loading:false})

        }).catch(error=>{
            alert("hata oluştu "+error)
            this.setState({loading:false})
        })
        
     


  }
//desen pattern  @ olacak . olacak noktadan sonra en az 2 haneli karakter
  validateEmail=(text)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text))
    {
      this.setState({email:text,error:""})
     
    }else{
      this.setState({error:"email formatını doğru giriniz..."})
    }
  }
  validatePsw=(text)=>{
    if(text.length>2){
      this.setState({password:text,error:""})
     
    }else{
      this.setState({error:"şifreniz en az 3 haneli olmalıdır."})
    }
  }
  validateUsername=(text)=>{
    if(text.length>2){
      this.setState({username:text,error:""})
     
    }else{
      this.setState({error:"Kullanıcı adınız en az 3 haneli olmalıdır."})
    }
  }

getData = async (ekstra) => {
  try {
    const value = await AsyncStorage.getItem('anahtar')
    if(value !== null) {
      this.key=value+ekstra
    }
  } catch(e) {
    this.key=""
  }
}

  writeData = async (value) => {
    try {
      await AsyncStorage.setItem('anahtar', value)
    } catch (e) {
      // saving error
    }
  }
  toStar=(text)=>{
      var s=""
      for(var i=0;i<text.length;i++)
      {
            s+="*"
      }
      return s
  }

  render() {

    const username = UserStore.username
    const email = UserStore.email
    const password = this.toStar(UserStore.password)



    if(this.state.loading)
      return (<View style={{alignItems:"center",justifyContent:"center",flex:1}}>
        <ActivityIndicator color={"red"} size={"large"}/>
        </View>)
    else
       return (
      <KeyboardAvoidingView style={{alignItems:"center",
      justifyContent:"center",flex:1}}>

          <Text>Binyıl Store Bilgi güncelleme</Text>
          <View>
            <Text style={{color:"red"}}>{this.state.error}</Text>
          </View>
          <View>
            <TextInput inlineImageLeft="person" style={styles.input} 
            onChangeText={(text)=>this.validateUsername(text)}
            placeholder={username} />
          </View>
          <View>
            <TextInput inlineImageLeft="email"  style={styles.input} 
            onChangeText={(text)=>this.validateEmail(text)}
            placeholder={email} />
          </View>
          <View>
            <TextInput  inlineImageLeft="psw"  style={styles.input}
            onChangeText={(text)=>this.validatePsw(text)}
          
            secureTextEntry={true}
            placeholder={password} />
          </View>
          <View>
            <TouchableOpacity style={styles.button} onPress={()=>this.userLogin()}>
              <Text style={{textAlign:"center",color:'white'}}>{"Güncelle"}</Text>
            </TouchableOpacity>
          </View>

      
         

      </KeyboardAvoidingView>
    )
  }
}
const styles=StyleSheet.create({
    input:{
      borderBottomWidth:1,
      borderBottomColor:"black",
      textAlign:"center",
      marginTop:10,
      width:300
    },
    button:{
      marginTop:20,
      borderRadius:15,
      borderWidth:1,
      paddingHorizontal:20,
      paddingVertical:5,
      width:300,
      marginBottom:15, 
      backgroundColor:'blue'
     
    },
    newRecord:{
      flexDirection:'row', 
      marginTop:10,
      height:30,
      justifyContent:'center', 
      alignItems:'center',
      marginBottom:10

     
    },
    ForgetPassword:{
      marginTop:5,
    }
});