import React, { Component } from 'react'
import { Text, View, TextInput, KeyboardAvoidingView,StyleSheet,
  ActivityIndicator, 
  Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import axios from 'axios';
import BASE from './constants';

export default class Register extends Component {
    state={
      loading:false,
      email:"",
      password:"",
      repassword:"",
      username:"",
      error:"",
      passed:false
    }

    registerUser = async(email,password,username)=>{

      this.setState({loading:true})      
      const params = new URLSearchParams();
      params.append("email",email)
      params.append("username",username)
      params.append("password",password)
      params.append("target","newUser")
      axios.post(BASE,params).then(res=>{         
         
          alert(JSON.stringify(res.data))          
          this.gotoBack();
          this.setState({loading:false})
        
      }).catch((error)=>{
        alert("Hata oluştu "+error)
        this.setState({loading:false})
      });

    }
    
    gotoBack=()=>{
        this.props.navigation.pop();
    }
  
  userRegister=()=>{

    if(this.state.password.length<1 || this.state.email.length<2 || this.state.username.length<2) return;

    this.registerUser(this.state.email,this.state.password,this.state.username)  


  }
//desen pattern  @ olacak . olacak noktadan sonra en az 2 haneli karakter
  validateEmail=(text)=>{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text))
    {
      this.setState({email:text,error:""})
     
    }else{
      this.setState({error:"email formatını doğru giriniz..."})
    }
  }
  validatePsw=(text)=>{
    if(text.length>2){
      this.setState({password:text,error:""})
     
    }else{
      this.setState({error:"şifreniz en az 3 haneli olmalıdır."})
    }
  }
  validatePswAgain=(text)=>{
    if(this.state.password.length == text.length && this.state.password.length>0){
      this.setState({passed:true,error:""})
     
    }else{
      this.setState({error:"şifre tekrarı uyuşmuyor..."})
    }
  }
  validateUsername=(text)=>{
    if(text.length>2){
      this.setState({username:text,error:""})
     
    }else{
      this.setState({error:"kullanıcı adınız en az 3 haneli olmalıdır."})
    }
  }





  render() {
    if(this.state.loading)
      return (<View style={{alignItems:"center",justifyContent:"center",flex:1}}>
        <ActivityIndicator color={"red"} size={"large"}/>
        </View>)
    else
       return (
      <KeyboardAvoidingView style={{alignItems:"center",
      justifyContent:"center",flex:1}}>

          <Text>Binyıl Store Register</Text>
          <View>
            <Text style={{color:"red"}}>{this.state.error}</Text>
          </View>
          <View>
            <TextInput inlineImageLeft="person" style={styles.input} 
            onChangeText={(text)=>this.validateUsername(text)}
            placeholder="Kullanıcı adı giriniz.." />
          </View>
          <View>
            <TextInput inlineImageLeft="email" style={styles.input} 
            onChangeText={(text)=>this.validateEmail(text)}
            placeholder="Email giriniz.." />
          </View>
          <View>
            <TextInput  inlineImageLeft="psw" style={styles.input}
            onChangeText={(text)=>this.validatePsw(text)}
          
            secureTextEntry={true}
            placeholder="Parola giriniz.." />
          </View>
          <View>
            <TextInput  inlineImageLeft="psw" style={styles.input}
            onChangeText={(text)=>this.validatePswAgain(text)}
          
            secureTextEntry={true}
            placeholder="Parola tekrarı giriniz.." />
          </View>
          <View>
            <TouchableOpacity style={styles.button} onPress={()=>this.userRegister()}>
              <Text style={{textAlign:"center",color:'white'}}>{"Kayıt ol"}</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.newRecord}>
          
            <TouchableOpacity onPress={this.gotoBack} style={{marginLeft:10,backgroundColor:'blue',borderRadius:5}}>
              <Text style={{color:'white',padding:5}}>Giriş Yap</Text>
            </TouchableOpacity>
          </View>
         
      </KeyboardAvoidingView>
    )
  }
}
const styles=StyleSheet.create({
    input:{
      borderBottomWidth:1,
      borderBottomColor:"black",
      textAlign:"center",
      marginTop:10,
      width:300
    },
    button:{
      marginTop:20,
      borderRadius:15,
      borderWidth:1,
      paddingHorizontal:20,
      paddingVertical:5,
      width:300,
      marginBottom:15, 
      backgroundColor:'blue'
     
    },
    newRecord:{
      flexDirection:'row', 
      marginTop:10,
      height:30,
      justifyContent:'center', 
      alignItems:'center',
      marginBottom:10

     
    },
    ForgetPassword:{
      marginTop:5,
    }
});