import React, { Component } from 'react';
import { View, Text ,FlatList,ActivityIndicator,Image} from 'react-native';
import { observer } from 'mobx-react';
import UserStore from "./UserStore"
import Axios from 'axios';
import BASE from "./constants"
import { TouchableOpacity } from 'react-native-gesture-handler';
import SepetStore from './SepetStore';
@observer
export default class Sepet extends Component {

  state={data:[],total:0,loading:true}

  componentDidMount()
  {
      this.props.navigation.addListener("focus",()=>{
        this.getSepetTotal()
      })
  }




  getSepetTotal=()=>{

    const _userId= UserStore.userId
    Axios.get(BASE,{params:{target:"sepetList",userId:_userId}}).then(result=>{
      
        const urunler=result.data
        var total=0
        if(urunler.length>0){
          
            urunler.forEach((urun)=>{
                const fiyat = urun.fiyat.replace(",",".")
                total +=parseFloat(fiyat)

            })

          
        }
        this.setState({data:result.data,total:total,loading:false})
        SepetStore.urunler=urunler;

    }).catch(error=>{
      alert("Hata oluştu"+error)
      this.setState({loading:false})
    })

  }

  deleteSepet=(urun)=>{

      const id=UserStore.userId
      Axios.get(BASE,{params:{target:"sepetCikar",userId:id,urunId:urun.id}}).then(res=>{

        const {result}=res.data
        if(result=="success")
        {
          const uruns = this.state.data.filter((ur)=>ur.id!=urun.id)
          this.state.total -=parseFloat(urun.fiyat.replace(",","."))
         
          this.setState({data:uruns})
          SepetStore.removeToBasket(urun)

        }
          

      }).catch(error=>{

      })



  }
  renderItem=({item,index})=>{

    return (
      <View style={{flexDirection:"row",backgroundColor:"white",marginTop:10,}}>
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Details",{urun:item})}>
          <View style={{margin:10}}>

            <Image source={{uri:item.urun_photo}} resizeMode="contain" style={{width:140,height:150}}/>
          </View>
          
        </TouchableOpacity>
        <View style={{margin:10}}>
          <Text style={{width:140}}>{item.urun_adi}</Text>
          <Text style={{width:140}}>{item.fiyat} {"₺"}</Text>
          <TouchableOpacity 
          onPress={()=>this.deleteSepet(item)}
          style={{backgroundColor:"orange",
          alignItems:"center",
          borderWidth:1,
          borderRadius:10,
          borderColor:"white"}}>
            <Text>{"Sil"}</Text>
          </TouchableOpacity>
        </View>

      </View>
    )
  }


  render() {

    if(this.state.total<=0)
    {
      return(<View><Text>{"Sepetiniz boş"}</Text></View>)

    }
         if(this.state.loading)
      {
        return(<View style={{alignItems:"center",
        justifyContent:"center",flex:1}}>
          <ActivityIndicator size={"large"}
           color={"red"} /></View>)
      }

    return (
      <View style={{alignItems:"center"}}>
        <View style={{height:"90%"}}>
          <FlatList 
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item)=>item.id}
          data={this.state.data}
          renderItem={this.renderItem}/>


        </View>
        <View style={{flexDirection:"row"}}>
          <Text>Sepet toplamı :</Text>
          <Text style={{marginLeft:10,fontSize:16}}>
            {this.state.total.toString().replace(".",",")} 
            {"₺"}</Text>

        </View>
      </View>
    );
  }
}
