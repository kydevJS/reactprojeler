import { observable, action } from "mobx";

class SepetStore {
    @observable urunler=[];
    
    @action
    getUrunAdet = ()=>  this.urunler.length

    @action
    addToBasket=(urun)=>{
        this.urunler.push(urun)
        
    }
    @action
    removeToBasket=(urun)=>{
        this.urunler=this.urunler.filter((ur)=>ur.id!=urun.id)
    }


}
export default new SepetStore()
