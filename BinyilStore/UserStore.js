import {observable,action} from "mobx";
class User {

    @observable username;
    @observable password;
    @observable userId;
    @observable email;   
    
    @action
    resetUser=()=>{
        this.username=""
        this.password=""
        this.userId=""
        this.email=""
    }

    @action 
    setUser(username,password,userId,email){
        this.username=username;
        this.password=password;
        this.userId=userId;
        this.email=email;
    }



}

export default new User();