import React, { Component } from 'react'
import { Text, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Anasayfa from './Anasayfa';
import Favori from './Favori';
import Profil from './Profil';
import Sepet from './Sepet';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome,faUserAlt,faHeart,faShoppingCart} from '@fortawesome/free-solid-svg-icons';
import HomeIconWithBadge from './IconWithBadge';
import { observer } from 'mobx-react';
import SepetStore from './SepetStore';
import axios from 'axios';
import BASE from './constants';
import UserStore from './UserStore';


const Tab = createBottomTabNavigator();
@observer
export default class HomeTab extends Component {
    componentDidMount()
    {

      this.props.navigation.addListener("focus",()=>{

        axios.get(BASE,{params:{"target":"sepetList","userId":UserStore.userId}}).then((res)=>{
          const dizi = res.data
          SepetStore.urunler=res.data
          

    }).catch(error=>{
          alert("hata oluştu "+error);
    })
      })
        
  



    }
    render() {
        return (
            <Tab.Navigator screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {                
                  if (route.name === 'Anasayfa') {                 
                
                  return <FontAwesomeIcon icon={faHome}  color={color} />;
                  }
                  if (route.name === 'Favori') {                  
                
                     return <FontAwesomeIcon icon={faHeart}  color={color} />;
                  }
                  if (route.name === 'Profil') {                  
                
                    return <FontAwesomeIcon icon={faUserAlt} color={color} />;
                  }
                  if (route.name === 'Sepet') {                  
                
                    return <HomeIconWithBadge color={color} badgeCount={SepetStore.urunler.length}/>;
                  }
      
                 
                },
              })}
              tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
                
              }}>
            <Tab.Screen name="Anasayfa" options={{tabBarLabel: 'Ürünler'}}  component={Anasayfa} />
            <Tab.Screen name="Favori" options={{tabBarLabel: 'Favori'}} component={Favori} />
            <Tab.Screen name="Profil" component={Profil} />
            <Tab.Screen name="Sepet" component={Sepet} />          
          </Tab.Navigator>
          

        )
    }
}
