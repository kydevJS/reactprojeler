/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Loading from "./Loading"
import Register from "./Register"
import React from 'react';
import PswRequest from "./ForgetPsw";
import HomeTab from "./HomeTab";

const Stack = createStackNavigator();

class App extends React.Component {

render()
{
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{header:()=>null}}>
                <Stack.Screen name="login" component={Loading} />
                <Stack.Screen name="register" component={Register} />
                <Stack.Screen name="pswForget" component={PswRequest} />
                <Stack.Screen name="home" component={HomeTab} />

            </Stack.Navigator>

        </NavigationContainer>
    )
}

}

export default App;
