import React from "react"
import {View, Text, Image, TouchableOpacity, ScrollView, BackHandler} from "react-native"
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faHeart} from '@fortawesome/free-solid-svg-icons'
import {observer} from "mobx-react"
import User from "./User"
import axios from "axios"
import BASE from "./Constants"
import SepetStore from "./SepetStore"
import FavoriteStore from "./FavoriteStore";

@observer
export default class Details extends React.Component {

    navigationOptions = {
        header: () => (<View style={{
                marginTop: 5,
                marginLeft: 5,
            }}>
                <TouchableOpacity onPress={this.goBack}>
                    <Image source={require("./img/back.png")} resizeMod={"contain"}/>
                </TouchableOpacity>
            </View>
        ),
    }
    state = {inFavs: false}
    goBack = async () => {
        await FavoriteStore.storeData()
        this.props.navigation.pop()
    }

    onBackPress=async ()=>{
        await FavoriteStore.storeData()
        this.props.navigation.pop()
        BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
    }
    componentDidMount() {
        this.props.navigation.setOptions(this.navigationOptions);
        const {urun} = this.props.route.params
        this.checkIfInFavs(urun)
        BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    }

    checkIfInFavs(urun) {
        var uruns = FavoriteStore.getUruns()
        if (uruns != null) {
            uruns.forEach((u) => {
                if (u.id == urun.id) {
                    this.setState({inFavs: true})
                }
            })
        }
    }

    addToBaskek = (urun) => {


        const {id} = User.getUser()

        axios.get(BASE, {
            params: {
                urunId: urun.id,
                fiyat: urun.fiyat,
                urunAdi: urun.urun_adi,
                userId: id,
                target: "sepeteEkle"
            }
        }).then((res) => {
            const {result} = res.data
            if (result == "success") {
                SepetStore.addBasket(urun.id)

                alert("Başarıyla sepete eklendi!")
            } else {
                alert("Üzgünüz eklenemedi. Lütfen tekrar deneyiniz!")
            }
        }).catch((err) => {
            alert(err.message)
        })


    }


    render() {

        const {urun} = this.props.route.params

        return (<ScrollView showsVerticalScrollIndicator={false} style={{marginHorizontal: 10,}}>
            <View style={{flexDirection: "row"}}>
                <View>
                    <TouchableOpacity  onPress={()=>{
                        var inFavs = this.state.inFavs
                        if (inFavs)
                        {
                            FavoriteStore.removeUrun(urun)
                        }else{
                            FavoriteStore.addUrun(urun)
                        }
                        this.setState({inFavs:!inFavs})

                    }}>
                    <Image source={{uri: urun.urun_photo}} resizeMode={"contain"} style={{width: 120, height: 200}}/>
                    </TouchableOpacity>
                    <View>
                        <TouchableOpacity style={{position: "absolute", marginLeft: 60, marginTop: -120}}>

                            <FontAwesomeIcon icon={faHeart} size={30} color={this.state.inFavs ? "red" : "gray"}
                                             onPress={() => {
                                                 FavoriteStore.addUrun(urun)
                                             }}
                            />


                        </TouchableOpacity>
                    </View>

                </View>
                <View style={{marginTop: 30}}>
                    <Text>{urun.urun_adi}</Text>
                    <Text>{urun.fiyat} {"₺"}</Text>
                    <TouchableOpacity style={{padding: 20,}} onPress={() => this.addToBaskek(urun)}><Text
                        style={{
                            width: 120,
                            textAlign: "center",
                            backgroundColor: "orange",
                            borderColor: "orange",
                            alignItems: "center",
                            borderWidth: 1,
                            borderRadius: 10,
                            marginTop: 20
                        }}>{"Sepete Ekle"}</Text>

                    </TouchableOpacity>


                </View>
            </View>
            <View>
                <Text>{urun.urun_aciklamasi}</Text>
            </View>

        </ScrollView>)
    }
}
