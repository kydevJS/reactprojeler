import React from 'react';
import {
    StyleSheet, FlatList, Platform,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions, Image
} from 'react-native';

import axios from "axios"
import BASE from "./Constants"
import {observer} from "mobx-react";
import FavoriteStore from "./FavoriteStore";

@observer
export default class Favoriler extends React.Component {

    state = {data: []}


    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("details",{urun:item})}>
                <View style={{margin:10}}>
                    <Image source={{uri:item.urun_photo}} resizeMode={"contain"} style={{width:250,height:260}}/>
                </View>
                <View style={{margin:10,alignItems:"center"}}>
                    <Text style={{width:250}}>{item.urun_adi}</Text>
                    <Text>{item.fiyat} {" ₺"}</Text>
                </View>


            </TouchableOpacity>


        )
    }

    componentDidMount() {

        const unsubscribe = this.props.navigation.addListener('focus', () => {
            // The screen is focused
            // Call any action
           const sepet = FavoriteStore.getUruns()
            if(sepet.length>0)
                this.setState({data:sepet})
            else
                this.setState({data:[]})
        });


    }


    render() {
        return (
            <View style={{alignItems:"center"}}>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    data={this.state.data}
                    renderItem={this.renderItem}
                /></View>)
    }
}
