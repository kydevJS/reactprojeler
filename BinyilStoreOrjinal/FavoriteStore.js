import {observable,action} from "mobx";
import AsyncStorage from '@react-native-community/async-storage'
class FavoriteStore{
    @observable uruns=[]
    @action
    setUruns(urunlist)
    {
        this.uruns=urunlist
    }
    @action
    addUrun(urun)
    {
        var isIn=false
        this.uruns.forEach((u)=>{
            if(u.id==urun.id) isIn=true
        })
        if(isIn==false)
        this.uruns.push(urun)
    }
    @action
    getUruns()
    {
        return this.uruns
    }
    @action
    removeUrun(urun)
    {
        this.uruns=this.uruns.filter((_urun)=>{
            return urun.id !=_urun.id
        })
    }


    @action
     storeData = async () => {
        try {

            if(this.uruns.length>0)
            {
                var str = ""
                this.uruns.forEach((urun)=>{
                    str +=urun.id+","
                })
                if(str!="")
                await AsyncStorage.setItem('@urunler',str)
            }

        } catch (e) {
            // saving error
        }
    }

    @action
    getData = async () => {
        try {
            const str = await AsyncStorage.getItem('@urunler')
            var back=[];
            if(str!="")
            {
                const lst = str.split(",")
                lst.forEach((dt)=>{
                    if(dt!="")
                    {
                        back.push(dt)
                    }
                })
            }
            return back
        } catch(e) {
            // error reading value
            return []
        }
    }


}
export default  new FavoriteStore()
