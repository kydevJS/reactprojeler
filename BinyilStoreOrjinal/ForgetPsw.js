/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet, Button,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions, Image
} from 'react-native';

import axios from "axios"
import BASE from "./Constants"

class PswRequest extends React.Component {

    navigationOptions = {
        header: () => (<View style={styles.headerStyle}>
                <TouchableOpacity onPress={this.goBack}>
                    <Image source={require("./img/back.png")} resizeMod={"contain"}/>
                </TouchableOpacity>
            </View>
        ),
    }

    goBack = () => {
        this.props.navigation.pop()
    }
    state = {email: "", loading: false}

    validEmail = (text) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
            this.setState({email: text})
        } else {
            this.setState({email: ""})
        }
    }

    componentDidMount() {
        this.props.navigation.setOptions(this.navigationOptions)
    }


    requestLogIn = (email) => {
        this.setState({loading: true})
        axios.get(BASE, {params: {"email": email, "target": "forgetPassword"}}).then((response) => {
            //alert(JSON.stringify(response))
            const {result} = response.data
            alert("Şifreniz :" + result)
            this.setState({loading: false})
        }).catch((error) => {
            print(error)
            this.setState({loading: false})
        });
    }

    logIn = () => {
        var email = this.state.email
        if (email != "") {

            this.requestLogIn(email)

        }
    }

    render() {

        var loading = this.state.loading;
        if (loading) {
            return (<View style={styles.mainPage}>
                <ActivityIndicator color={"#0000ff"} size={"small"}/>
            </View>)
        } else {
            return (
                <KeyboardAvoidingView style={styles.mainPage}>

                    <Text>{"Şifre Sıfırlama"}</Text>
                    <View>
                        <TextInput keyboardType={"email-address"} inlineImageLeft='search'
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.validEmail(text)}
                                   placeholder={"Email giriniz."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.logIn()} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>{"Şifremi sıfırla"}</Text>
                        </TouchableOpacity>
                    </View>


                </KeyboardAvoidingView>
            )
        }
    }

}

const styles = StyleSheet.create({
    mainPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    headerStyle: {
        marginTop: 10,
        marginLeft: 5,
    }
    , inputStyle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 300,
        marginTop: 20,
        height: 40,
        textAlign: "center",
        borderColor: "gray",

    },
    textStyle: {
        textAlign: "center",
        color: "white",
        fontSize: 16,
    },
    buttonStyle: {
        borderRadius: 10,
        marginTop: 20,
        padding: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue"
    }
});

export default PswRequest;
