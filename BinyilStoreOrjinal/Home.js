import React from "react"
import User from "./User";
import {observer} from "mobx-react";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {createStackNavigator} from "@react-navigation/stack";
import Urun from "./Urun";
import Sepet from "./Sepet";
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome'
import {faHome, faUserAlt, faHeart, faShoppingCart} from '@fortawesome/free-solid-svg-icons'
import Profile from "./Profile";
import Favoites from "./Favoriler";
import HomeIconWithBadge from "./Badget";

import SepetStore from "./SepetStore";
import BASE from "./Constants";
import axios from "axios"

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
@observer
export default class Home extends React.Component {
    getSepetTotal = () => {
        const {id} = User.getUser()
        axios.get(BASE, {params: {userId: id,target:"sepetList"}}).then((resp) => {
            if(resp.data.length!=undefined)
            SepetStore.setTotalItems(resp.data.length)
        })
    }

    componentDidMount() {
        const unsubscribe = this.props.navigation.addListener('focus', () => {
            // The screen is focused
            // Call any action
            const total = SepetStore.getTotal()
            this.setState({total: total})
        });
        this.getSepetTotal()
    }

    state = {total: 0}

    render() {


        return (
            <Tab.Navigator screenOptions={({route}) => ({
                tabBarIcon: ({focused, color, size}) => {

                    if (route.name === 'urunler') {

                        if (focused) {
                            return <FontAwesomeIcon icon={faHome} color={color}/>
                        } else {
                            return <FontAwesomeIcon icon={faHome} color={color}/>
                        }

                    } else if (route.name === 'favoriler') {
                        if (focused) {
                            return <FontAwesomeIcon icon={faHeart} color={color}/>
                        } else {
                            return <FontAwesomeIcon icon={faHeart} color={color}/>
                        }
                    } else if (route.name === 'profile') {
                        if (focused) {
                            return <FontAwesomeIcon icon={faUserAlt} color={color}/>
                        } else {
                            return <FontAwesomeIcon icon={faUserAlt} color={color}/>
                        }
                    } else if (route.name === 'sepet') {
                        return (
                            <HomeIconWithBadge
                                color={color}
                                badgeCount={SepetStore.totalItems}
                            />)

                    }

                },
            })}
                           tabBarOptions={{
                               activeTintColor: 'tomato',
                               inactiveTintColor: 'gray',
                               showIcons: true
                           }}
            >
                <Tab.Screen name="urunler" component={Urun}/>
                <Tab.Screen name="favoriler" component={Favoites}/>
                <Tab.Screen name="profile" component={Profile}/>
                <Tab.Screen name="sepet" component={Sepet}/>

            </Tab.Navigator>
        )
    }

}
