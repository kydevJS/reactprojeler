import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Home from "./Home";
import ProfileUpdate from "./ProfileUpdate";
import Details from "./Details";

const Stack = createStackNavigator();

class App extends React.Component {

    render() {
        return (

            <Stack.Navigator screenOptions={{header: () => null}}>
                <Stack.Screen name="home" component={Home}/>
                <Stack.Screen name={"profileUpdate"} component={ProfileUpdate}/>
                <Stack.Screen name={"details"} component={Details}/>
            </Stack.Navigator>


        )
    }

}

export default App;
