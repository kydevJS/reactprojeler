/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {observer} from "mobx-react"
import axios from "axios"
import BASE, {generateGet} from "./Constants"
import UserStore from "./User"

@observer
class Loading extends React.Component {

    state = {email: "", uid: "", photo: "", loading: false, error: ""}
    width = Dimensions.get("window").width
    height = Dimensions.get("window").height
    userStore = UserStore



    validEmail = (text) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
            this.setState({email: text})
        } else {
            this.setState({email: "", error: "Email formatı hatalı"})
        }
    }
    validate = (text) => {
        if (text.length > 4) {
            this.setState({password: text})

        } else {
            this.setState({password: "", error: "Şifre en az 4 haneli olmalı"})

        }
    }
    read = async (email, psw) => {
        await AsyncStorage.setItem("@email", email)
        await AsyncStorage.setItem("@password", psw)
        this.props.navigation.replace("home")


    }


    componentDidMount() {

       this.checkUser()
    }

    gotoPage(name) {
        this.props.navigation.navigate(name);
    }

    gotoForgetPsw = () => {
        this.gotoPage("pswForget")
    }
    gotoRegister = () => {
        this.props.navigation.navigate("register");
    }
    checkUser = async () => {
        this.setState({loading: true})
        var email = await AsyncStorage.getItem("@email")
        var password = await AsyncStorage.getItem("@password")
        if (email != null && password != null) {

            this.checkToLogin(email, password)
        }else{
            this.setState({loading: false})
        }
    }
    requestLogIn = (email, password) => {
        this.setState({loading: true})
        //var params = generateGet(["email="+email,"password="+password,"target="+"login"])
        axios.get(BASE, {params: {email: email, password: password, target: "login"}}).then((response) => {
            var {username, id} = response.data
            this.userStore.setUser(username, email, password, id)
            //this.setState({loading: false})
            this.read(email,password)
        }).catch((error) => {
            this.setState({loading: false, error: "Bağlantı yapılmadı. Lütfen internete bağlı olduğunuzdan emin olun!"})
        });
    }

    checkToLogin = (email, password) => {
        this.setState({loading: true})
        //var params = generateGet(["email="+email,"password="+password,"target="+"login"])
        axios.get(BASE, {params: {email: email, password: password, target: "login"}}).then((response) => {
            var {username, id} = response.data
            this.userStore.setUser(username, email, password, id)
            this.gotoPage("home")
           // this.setState({loading: false})
        }).catch((error) => {
            this.setState({loading: false, error: "Bağlantı yapılmadı. Lütfen internete bağlı olduğunuzdan emin olun!"})
        });
    }
    logIn = () => {
        var email = this.state.email
        var password = this.state.password
        if (email != "" && password != "") {

            this.requestLogIn(email, password)

        }
    }

    render() {

        var loading = this.state.loading;
        if (loading) {
            return (<View style={styles.mainPage}>
                <ActivityIndicator color={"#0000ff"} size={"small"}/>
            </View>)
        } else {
            return (
                <KeyboardAvoidingView style={styles.mainPage}>

                    <Text>{"Ucuncubinyıl Store"}</Text>
                    <View>
                        <Text>{this.state.error}</Text>
                    </View>
                    <View>
                        <TextInput keyboardType={"email-address"} inlineImageLeft='email'
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.validEmail(text)}
                                   placeholder={"Email giriniz..."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput inlineImageLeft='psw' placeholder={"parola giriniz..."} returnKeyLabel={"Giriş"}
                                   secureTextEntry={true} enablesReturnKeyAutomatically={true}
                                   onChangeText={(text) => this.validate(text)} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.logIn()} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>{"Giriş Yap"}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.account}>
                        <Text>Yoksa hesabınız yok mu?</Text>
                        <TouchableOpacity style={styles.buttonStyle2} onPress={() => this.gotoRegister()
                        }>
                            <Text style={styles.textStyle}>Kaydol</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.pswStyle}>
                        <TouchableOpacity onPress={() => this.gotoForgetPsw()}>
                            <Text>
                                Şifremi Unuttum
                            </Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            )
        }
    }

}

const styles = StyleSheet.create({
    mainPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    account: {
        flexDirection: "row",
        height: 30,
        marginTop: 20,
        justifyContent: "space-evenly"
    }, inputStyle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 300,
        marginTop: 20,
        height: 40,
        textAlign: "center",
        borderColor: "gray"

    },
    textStyle: {
        textAlign: "center",
        color: "white",
        fontSize: 16,
    },
    buttonStyle: {
        borderRadius: 10,
        marginTop: 20,
        padding: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue"
    }, buttonStyle2: {
        borderRadius: 10,
        paddingHorizontal: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue",
        marginLeft: 20,
    },
    pswStyle: {
        marginTop: 20,
    }
});

export default Loading;
