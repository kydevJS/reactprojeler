/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet, Button, Image,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {observer} from "mobx-react"
import axios from "axios"
import BASE from "./Constants"
import User from "./User"

@observer
class Profile extends React.Component {
componentDidMount()
{
    const unsubscribe = this.props.navigation.addListener('focus', () => {
        // The screen is focused
        // Call any action
        this.setState({})
    });
}

    exitApp=()=>{
    User.reset()
    this.props.navigation.replace("login")
    }
    gotoProfileUpdate=()=>{
        this.props.navigation.navigate("profileUpdate")
    }
    render() {

        const {username,email}=User.getUser()
            return (
                <KeyboardAvoidingView style={styles.mainPage}>

                    <Text style={styles.profileStyle}>{"Profil"}</Text>

                    <View>
                       <Text style={styles.textStyle}>{username}</Text>
                    </View>
                    <View>
                        <Text style={styles.textStyle}>{email}</Text>
                    </View>

                    <View style={styles.pswStyle}>
                        <TouchableOpacity onPress={this.exitApp}>
                            <Text style={[styles.textStyle,styles.red]}>
                                Çıkış yap
                            </Text>
                        </TouchableOpacity>
                    </View><View style={styles.pswStyle}>
                        <TouchableOpacity onPress={this.gotoProfileUpdate}>
                            <Text style={[styles.textStyle,styles.blue]}>
                                Değiştir
                            </Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            )
        }


}

const styles = StyleSheet.create({
    mainPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    blue:{
      color:"white",
        fontSize: 20,
        marginVertical: 50,
        backgroundColor: "black"
    }, red:{
      color:"white",
        fontSize: 18,
        marginVertical: 10,
        backgroundColor: "red",
        borderColor:"red"
    },
    textStyle: {
        textAlign: "center",
        color: "white",
        fontSize: 16,
        backgroundColor:"gray",
        width:300,
        borderRadius:10,
        borderWidth:1,
        marginVertical:10,
    },
    profileStyle:{
      marginVertical:30,
        fontSize:25,
        fontWeight:"bold",
        color:"blue",

    },
    pswStyle: {
        marginTop: 20,
    }
});

export default Profile;
