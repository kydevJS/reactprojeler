/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet, Button, Image,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {observer} from "mobx-react"
import axios from "axios"
import BASE from "./Constants"
import User from "./User";

@observer
class ProfileUpdate extends React.Component {

    navigationOptions = {
        header: () => (<View style={styles.headerStyle}>
                <TouchableOpacity onPress={this.goBack}>
                    <Image source={require("./img/back.png")} resizeMod={"contain"}/>
                </TouchableOpacity>
            </View>
        ),
    }
    state = {
        email: "",
        id: "",
        loading: false,
        passwordAgain: true,
        psw: "",
        error: "",
        username: "",
        password: "",
    }
    goBack = () => {
        this.props.navigation.pop()
    }


    validEmail = (text) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
            this.setState({email: text})
        } else {
            this.setState({email: ""})
        }
    }
    validate = (text) => {
        if (text.length > 4) {
            this.setState({username: text})


        } else {
            this.setState({username: ""})
        }
    }



    componentDidMount() {
        this.props.navigation.setOptions(this.navigationOptions);
        const {username,email,password}=User.getUser()
        this.setState({email: email,username: username,password:password})

    }



    requestUpdate = (email, password, username) => {

        this.setState({loading: true})
        const {id}=User.getUser()
        const params = new URLSearchParams();
        params.append('email', email);
        params.append('username', username);
        params.append('password', password);
        params.append('target', "updateUser");
        params.append("userId",id)
        axios.post(BASE, params).then((res) => {
            const {result} = res.data
            if(result=="success")
            {
                User.setUser(username,email,password,id)
                User.read(email,password)
                this.goBack()
            }else{

            }
            this.setState({loading: false})
           // this.goBack()

        }).catch((a) => {
            this.setState({loading: false})
            alert("Hata oluştu.")

        })


    }

    logIn = () => {
        var email = this.state.email
        var password = this.state.password
        var username = this.state.username
        if (email != "" && password != "" && username != "") {

            this.requestUpdate(email, password, username)

        } else {
            this.setState({error: "Formu doldurmadınız!"})
        }
    }


    render() {
        var loading = this.state.loading
        if (loading) {
            return (<View style={styles.mainPage}>
                <ActivityIndicator color={"#0000ff"} size={"small"}/>
            </View>)
        } else {
            return (
                <KeyboardAvoidingView style={styles.mainPage}>

                    <Text>{"Ucuncubinyıl Store Profil Güncelleme"}</Text>
                    <View>
                        <Text style={styles.errorDisplay}>{this.state.error}</Text>
                    </View>
                    <View>
                        <TextInput inlineImageLeft='person'

                                   value={this.state.username}
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.setState({username:text})}
                                   placeholder={"Kullanıcı adı giriniz."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput keyboardType={"email-address"} inlineImageLeft='email'
                                   value={this.state.email}
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.setState({email:text})}
                                   placeholder={"Email giriniz."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput
                            inlineImageLeft='psw'
                            value={this.state.password}
                            placeholder={"Parola giriniz..."}
                            secureTextEntry={true} enablesReturnKeyAutomatically={true}
                            onChangeText={(text) => this.setState({passwors:text})} style={styles.inputStyle}/>
                    </View>

                    <View>
                        <TouchableOpacity onPress={() => this.logIn()} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>{"Güncelle"}</Text>
                        </TouchableOpacity>
                    </View>


                </KeyboardAvoidingView>
            )
        }
    }

}

const styles = StyleSheet.create({
    mainPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    inputStyle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 300,
        marginTop: 20,
        height: 40,
        textAlign: "center",
        borderColor: "gray",

    },
    errorDisplay: {
        color: "red",
        fontStyle: "italic"
    },
    headerStyle: {
        marginTop: 10,
        marginLeft: 5,
    },
    textStyle: {
        textAlign: "center",
        color: "white",
        fontSize: 16,
    },
    buttonStyle: {
        borderRadius: 10,
        marginTop: 20,
        padding: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue"
    }, buttonStyle2: {
        borderRadius: 10,
        paddingHorizontal: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue",
        marginLeft: 20,
    },
    pswStyle: {
        marginTop: 20,
    }
});

export default ProfileUpdate;
