/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet, Button, Image,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {observer} from "mobx-react"
import axios from "axios"
import BASE from "./Constants"

@observer
class Register extends React.Component {

    navigationOptions = {
        header: () => (<View style={styles.headerStyle}>
                <TouchableOpacity onPress={this.goBack}>
                    <Image source={require("./img/back.png")} resizeMod={"contain"}/>
                </TouchableOpacity>
            </View>
        ),
    }
    state = {
        email: "abc@abc.com",
        id: "",
        loading: false,
        passwordAgain: true,
        psw: "",
        error: "",
        username: "ccccc",
        password: "12345678"
    }
    goBack = () => {
        this.props.navigation.pop()
    }


    validEmail = (text) => {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(text)) {
            this.setState({email: text})
        } else {
            this.setState({email: ""})
        }
    }
    validate = (text) => {
        if (text.length > 4) {
            this.setState({username: text})

        } else {
            this.setState({username: ""})
        }
    }
    validate2 = (text) => {
        if (text.length > 3) {
            this.setState({password: text})

        } else {
            this.setState({password: ""})
        }
    }


    componentDidMount() {
        this.props.navigation.setOptions(this.navigationOptions);


    }


    requestLogIn = (email, password, username) => {
        if (this.state.passwordAgain == false) {
            this.setState({error: "Şifreler uyuşmuyor!"})
            return
        }
        this.setState({loading: true})

        const params = new URLSearchParams();
        params.append('email', email);
        params.append('username', username);
        params.append('password', password);
        params.append('target', "newUser");
        axios.post(BASE, params).then((res) => {
            const {userId} = res.data
            alert("Başarıyla kaydınız yapıldı")
            this.setState({loading: false})
            this.goBack()

        }).catch((a) => {
            this.setState({loading: false})
            alert("Hata oluştu.")

        })


    }

    logIn = () => {
        var email = this.state.email
        var password = this.state.password
        var username = this.state.username
        if (email != "" && password != "" && username != "") {

            this.requestLogIn(email, password, username)

        } else {
            this.setState({error: "Formu doldurmadınız!"})
        }
    }

    checkEqual = (text) => {
        if (text != "") {
            if (text.length > 3) {
                alert(text + " == " + this.state.password)

                if (text == this.state.password) {
                    this.setState({passwordAgain: true})
                    this.setState({psw: ""})
                } else {
                    this.setState({passwordAgain: false})
                    this.setState({psw: "Şifreler uyuşmuyor."})
                }
            } else {
                this.setState({passwordAgain: false})
                this.setState({psw: "Şifreler uyuşmuyor."})
            }
        } else {
            this.setState({passwordAgain: false})
            this.setState({psw: "Şifreler uyuşmuyor."})
        }
    }

    render() {

        var loading = this.state.loading;
        if (loading) {
            return (<View style={styles.mainPage}>
                <ActivityIndicator color={"#0000ff"} size={"small"}/>
            </View>)
        } else {
            return (
                <KeyboardAvoidingView style={styles.mainPage}>

                    <Text>{"Ucuncubinyıl Store Katılım"}</Text>
                    <View>
                        <Text style={styles.errorDisplay}>{this.state.error}</Text>
                    </View>
                    <View>
                        <TextInput inlineImageLeft='person'
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.validate(text)}
                                   placeholder={"Kullanıcı adı giriniz."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput keyboardType={"email-address"} inlineImageLeft='email'
                                   enablesReturnKeyAutomatically={true} onChangeText={(text) => this.validEmail(text)}
                                   placeholder={"Email giriniz."} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput
                            inlineImageLeft='psw'
                            placeholder={"Parola giriniz..."}
                            secureTextEntry={true} enablesReturnKeyAutomatically={true}
                            onChangeText={(text) => this.validate2(text)} style={styles.inputStyle}/>
                    </View>
                    <View>
                        <TextInput
                            inlineImageLeft='psw'
                            placeholder={"Parola tekrarı ...."}
                            secureTextEntry={true} enablesReturnKeyAutomatically={true}
                            onChangeText={(text) => this.checkEqual(text)} style={styles.inputStyle}/>
                        <Text style={styles.errorDisplay}>{this.state.psw}</Text>
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => this.logIn()} style={styles.buttonStyle}>
                            <Text style={styles.textStyle}>{"Kayıt Ol"}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.pswStyle}>
                        <TouchableOpacity onPress={this.goBack}>
                            <Text>
                                Hesabım Var
                            </Text>
                        </TouchableOpacity>
                    </View>

                </KeyboardAvoidingView>
            )
        }
    }

}

const styles = StyleSheet.create({
    mainPage: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    inputStyle: {
        borderRadius: 10,
        borderWidth: 1,
        width: 300,
        marginTop: 20,
        height: 40,
        textAlign: "center",
        borderColor: "gray",

    },
    errorDisplay: {
        color: "red",
        fontStyle: "italic"
    },
    headerStyle: {
        marginTop: 10,
        marginLeft: 5,
    },
    textStyle: {
        textAlign: "center",
        color: "white",
        fontSize: 16,
    },
    buttonStyle: {
        borderRadius: 10,
        marginTop: 20,
        padding: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue"
    }, buttonStyle2: {
        borderRadius: 10,
        paddingHorizontal: 10,
        backgroundColor: "blue",
        borderWidth: 1,
        borderColor: "blue",
        marginLeft: 20,
    },
    pswStyle: {
        marginTop: 20,
    }
});

export default Register;
