import React from 'react';
import {
    StyleSheet, FlatList, Platform,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions, Image
} from 'react-native';

import axios from "axios"
import BASE from "./Constants"
import FavoriteStore from "./FavoriteStore";
import {observer} from "mobx-react";
import User from "./User";
import SepetStore from "./SepetStore";

@observer
export default class Sepet extends React.Component {

    /*
    if($tgt=="sepetCikar")
{
    $urunId=getVal("urunId");
    $userId

     */
    state = {data: [],total:0}
    delFromSepet=(urun)=>{
        const {id} = User.getUser()
        axios.get(BASE,{params:{target:"sepetCikar","urunId":urun.id,"userId":id}}).then((resp)=>{

            const total = this.state.total-parseFloat(urun.fiyat.replace(",","."))
            const data =  this.state.data.filter((dt)=>dt.id!=urun.id)
            const cntTotal = data.length
            if(cntTotal!=undefined)
                SepetStore.setTotalItems(cntTotal)
            this.setState({data:data,total:total.toFixed(2)})
        }).catch((err)=>{
            alert(err)
        })

    }
    getSepetTotal = () => {
        const {id} = User.getUser()
        axios.get(BASE, {params: {userId: id,target:"sepetList"}}).then((resp) => {
            const data = resp.data
            var sum=0
            if(data.length!=undefined)
            {
                data.forEach((dt)=>{
                    sum += parseFloat(dt.fiyat.replace(",","."))

                })
            }

            this.setState({data: resp.data,total:sum.toFixed(2)})
        })
    }


    componentDidMount() {
        const unsubscribe = this.props.navigation.addListener('focus', () => {
            // The screen is focused
            // Call any action
            this.getSepetTotal()
        });

    }

    renderItem = ({item, index}) => {
        return (
            <View style={{flexDirection:"row"}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("details", {urun: item})}>
                <View style={{margin: 10}}>
                    <Image source={{uri: item.urun_photo}} resizeMode={"contain"} style={{width: 140, height: 150}}/>
                </View>
                </TouchableOpacity>
                <View style={{margin: 10}}>
                    <Text style={{width: 140}}>{item.urun_adi}</Text>
                    <Text>{item.fiyat} {" ₺"}</Text>
                    <TouchableOpacity style={{backgroundColor:"orange",alignItems:"center",
                        marginVertical:10,
                        borderColor:"white",
                        borderWidth:1,
                        borderRadius:10}} onPress={()=>this.delFromSepet(item)}>
                        <Text>{"Sil"}</Text></TouchableOpacity>
                </View>

            </View>


        )
    }


    render() {
        return (
            <View style={{alignItems: "center"}}>
                <View style={{height:"90%"}}>
                <FlatList
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    data={this.state.data}
                    renderItem={this.renderItem}
                />
                </View>
                <View style={{flexDirection:"row"}}>
                    <Text>Sepet toplamı :</Text>
                    <Text>{this.state.total}{"₺"}</Text>

                </View>
            </View>)
    }
}
