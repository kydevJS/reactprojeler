import {observable,action} from "mobx";

class SepetStore {
    @observable urunIds = []
    @observable totalItems = 0

    @action
    setTotalItems(itm)
    {
        this.totalItems=itm
    }
    @action
    addBasket(id) {
        this.urunIds.push(id)
        this.totalItems += 1
    }

    @observable
    removeBasket(id) {
        this.urunIds.filter((item)=>item.id!=id)
        this.totalItems-=1

    }
    @action
    getTotal()
    {
        return this.totalItems
    }
    @action
    getIds()
    {
        return this.urunIds
    }
}
export default new SepetStore()
