import React from 'react';
import {
    StyleSheet, FlatList, Platform,
    View, TouchableOpacity,
    Text, TextInput, KeyboardAvoidingView, ActivityIndicator, Dimensions, Image
} from 'react-native';

import axios from "axios"
import BASE from "./Constants"
import FavoriteStore from "./FavoriteStore";
import {observer} from "mobx-react";
import AsyncStorage from '@react-native-community/async-storage'
@observer
export default class Urun extends React.Component {

    state = {data: []}
    renderItem = ({item, index}) => {
        return (
            <TouchableOpacity onPress={()=>this.props.navigation.navigate("details",{urun:item})}>
               <View style={{margin:10}}>
                <Image source={{uri:item.urun_photo}} resizeMode={"contain"} style={{width:140,height:150}}/>
                </View>
                <View style={{margin:10}}>
                <Text style={{width:140}}>{item.urun_adi}</Text>
                <Text>{item.fiyat} {" ₺"}</Text>
                </View>


            </TouchableOpacity>


        )
    }

    dataGet=async (urunler) =>{
        var favorites = await FavoriteStore.getData()
        var toSet=[]
        urunler.forEach((urun)=>{
            favorites.forEach((id)=>{
                if(id==urun.id){
                    toSet.push(urun)
                }
            })
        })
        FavoriteStore.setUruns(toSet)



    }
    componentDidMount() {
        axios.get(BASE, {params: {target: "listUruns"}}).then(response => {


            this.dataGet(response.data)
            this.setState({data: response.data})

        }).catch((err) => alert(err.message))

    }


    render() {
        return (
            <View style={{alignItems:"center"}}>


               <FlatList
                   showsHorizontalScrollIndicator={false}
                   showsVerticalScrollIndicator={false}
                   numColumns={2}
                keyExtractor={(item) => item.id}
                data={this.state.data}
                renderItem={this.renderItem}
            /></View>)
    }
}
