
//class UserStore

import {action, observable,computed} from "mobx";
import AsyncStorage from "@react-native-community/async-storage";

class UserStore {
    @observable username=""
    @observable email=""
    @observable password=""
    @observable id=""

    @action
    reset()
    {
        this.username=""
        this.email=""
        this.password=""
        this.id=""
        this.del()
    }
    @action
    setUser(username,email,password,id){
        this.username=username
        this.email=email
        this.password=password
        this.id=id
    }
    @action
    getUser()
    {
       return {"username":this.username,"email":this.email,"id":this.id,"password":this.password}
    }

    read = async (email, psw) => {
        await AsyncStorage.setItem("@email", email)
        await AsyncStorage.setItem("@password", psw)



    }
    del = async () => {
        await AsyncStorage.setItem("@email", "")
        await AsyncStorage.setItem("@password", "")



    }


}

export default new UserStore();
