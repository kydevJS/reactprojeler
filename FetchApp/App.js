import React from 'react';
import {
  View,Text, ScrollView, Image,TouchableOpacity,Linking
} from 'react-native';


class App extends React.Component {
  apiKey="api.openweathermap.org/data/2.5/weather?appid=1df40a009087ebb29c50af89e36d5989&q=";

  
  DATA=[]
  state={data:this.DATA}
  constructor(prop)
  {
    super(prop)
    this.getDataFromWeb()
  }
 
  buy=(link)=>{
   
     Linking.openURL(link)
  }
  async getDataFromWeb()
  {//yöntem 1 react native fetch

    var url="http://sharer.tk/movei.php"
    await fetch(url).then(response=>response.json()).then(response =>{
      response.forEach(item=>{
        this.DATA.push(item)
      })
      this.setState({data:this.DATA})
      })

  }
  /*async getDataWithAsync(){
    var url="http://sharer.tk/movei.php";
    var data=await fetch(url);
    var response=await data.json();
    response.forEach(item=>{
      alert(item.title+"..."+item.artist);
    })
  }*/
  //GET, POST, 

  /*getWithHttp()
  {
      var url="http://avcilar.bel.tr/apiAvcilar/api.php";//"http://sharer.tk/movei.php";
      var request=new XMLHttpRequest();
      request.onreadystatechange=(e)=>{

        if(request.readyState!==4) { return; }//geri dön hala yüklenmedi site
        if(request.status==200)
        {
          alert(request.responseText)
        }

      }
      request.open("POST",url)
      request.send();


  }*/
  render() {

    return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={{alignItems:"center"}}>
        {
          this.state.data.map((item,index)=>{
            return (<View key={index}> 
              <View style={{flexDirection:"row"}}>
                    <Image source={{uri:item.thumbnail_image}} 
                        style={{width:30,height:30,resizeMode:"center"}} /> 
                    <Text>{item.title}</Text>
                </View>
               <Image source={{uri:item.image}} 
               style={{width:300,height:300,resizeMode:"stretch"}}/>
               <TouchableOpacity
               onPress={()=>this.buy(item.url)}               
               style={{alignItems:"center",
               backgroundColor:"red",
               marginHorizontal:20,
               marginVertical:30,
               borderRadius:25,
               padding:10,               
              }}>
                 <Text style={{fontSize:18,fontWeight:"bold", color:"white"}}>Satın Al</Text>
               </TouchableOpacity>

            </View>)

          })
        }
      </View>
      </ScrollView>
    );
  }

}
export default App;
