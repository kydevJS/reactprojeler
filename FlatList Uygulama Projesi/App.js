/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  View,
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  TextInput
} from 'react-native';

class App extends React.Component {

  url="https://randomuser.me/api/?results=60";
  state={data:[]}
  constructor(prop) {
    super(prop);
    this.DATA=[]
   fetch(this.url).then(response=>response.json()).then(response=>{
      //alert(JSON.stringify(response))
      //alert(response.results)
      this.setState({data:response.results})
      this.DATA=response.results
      //response.results.map(item=>alert(JSON.stringify(item.name.first)))

    });

   
  }
  renderItem=(item,index)=>{
      //alert(JSON.stringify(item))
    return (<TouchableOpacity style={{flexDirection:"row"}}>
      <Image style={{width:50,height:50,borderRadius:25,marginLeft:5}} 
      source={{uri:item.item.picture.thumbnail}}/>
      <View style={{marginHorizontal:5}}>
          <Text >
       {item.item.name.first} {item.item.name.last}
      </Text>
      <Text style={{fontSize:10}}>{item.item.email}</Text>
      </View>
    
      </TouchableOpacity>)
  }
  headerComponent=()=>{
    return <View  style={{backgroundColor:"white"}}>
      <TextInput
    style={{borderRadius:20,borderWidth:1,
      paddingLeft:10,
      borderColor:"red",
    margin:10}}
     placeholder={"Arama yap"} 
    
    onChangeText={this.searchText} />
    </View>
  }
  searchText=text=>{
   
    const yeniData=this.DATA.filter(item=>{
      const aramaYapilacakMetin=item.name.first+" "+item.name.last;
      return aramaYapilacakMetin.toLowerCase().indexOf(text.toLowerCase())>=0

    })
    this.setState({data:yeniData})

  }

  render() {
    return (
      <FlatList
      ListHeaderComponent={this.headerComponent}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item,index)=>item.login.uuid}
      data={this.state.data}
      renderItem={this.renderItem}
      stickyHeaderIndices={[0]}
      />);
  }
}

export default App;
