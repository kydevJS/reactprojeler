/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';


import * as React from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import ProfileScreen from './ProfileScreen';

function HomeScreen({navigation,route}) {

  const [isim,setIsim]=React.useState("yahya");
  return (   
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
      <Text>{route.params.name}</Text>
      <Text>{route.params.yas}</Text>
      <Text>{isim}</Text>
      <Button title={"değiştir"} onPress={()=>setIsim("selim")}/>
      <Button title={"Detay sayfasına git"} onPress={()=>navigation.navigate("Detay")}/>
    </View>
  );
}

DetailScreen=({navigation})=>{
  return(<View>
    <Text>Detay</Text>
    <Button title={"Profile Git"} onPress={()=>navigation.navigate("Profil")}/>
  </View>)
}

/*
<a href="index.php"/>
*/
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Detay" component={DetailScreen} options={{title:"Benim Detayım"}} />
        <Stack.Screen name="Profil" component={ProfileScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
