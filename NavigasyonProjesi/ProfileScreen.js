import React, { Component } from 'react'
import { Text, View,Button } from 'react-native'

export default class ProfileScreen extends Component {
    
    render() {
        const navigation=this.props.navigation;
        return (
            <View>
                <Text> Profile screen </Text>
                <Button title={"Anasayfaya git"} 
                onPress={()=>navigation.navigate("Home",{name:"Aytekin",yas:"19"})}/>
            </View>
        )
    }
}
