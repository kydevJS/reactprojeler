import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class PagerApp extends Component {
    state={isPage1:true}
handle=(page)=>{
  this.setState({isPage1:page})
}
    render() {
        if(this.state.isPage1)
    {
      return <Page1 handle={this.handle} />;
    }else{
      return <Page2 handle={this.handle} />;
    }
    }
}
