import React, { Component } from 'react'
import { Text, View } from 'react-native'
import PropTypes from "prop-types";
export default class MButton extends Component {
    
    render() {
        const {metin,sayi,handle}=this.props;
        return (
            <View>
                <Text onPress={handle}> {metin} </Text>
                <Text>{sayi}</Text>
            </View>
        )
    }
}
MButton.propTypes={
    metin:PropTypes.oneOfType([PropTypes.string.isRequired,
        PropTypes.object]),
    sayi:PropTypes.number

};