/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  ImageBackground,
  Animated,
  Image,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';
//enemy top ve left'e göre gidiyor
//füze bottom ve translatex değerine gidiyor
//bu değerleri karşılaştırmak gerek veya aynı
//türe getirmek gerekmektedir.
//ayrıca loop olayına bakmak gerekmektedir.

class App extends React.Component {
	
  _width = Dimensions.get('window').width;
  _height = Dimensions.get('window').height;
  canFire = true;
  constructor(props) {
    super(props);
    setInterval(this.randomEnemy, 3000);
    setInterval(this.checker,10);

  }
checker=()=>{




}
  randomEnemy = () => {
    const leftVal = Math.random();
    const topVal = -50;
    const left = this._width * leftVal;
    this.setState({enemyLeft: left});
    Animated.timing(this.state.enemyTop, {
      toValue: this._height * 1.2,
      duration: 2000,
    }).start(() => {
      Animated.timing(this.state.enemyTop, {
        toValue: topVal,
        duration: 10,
      }).start();
    });
  };

  fireMissile = xPos => {
    Animated.timing(this.state.missilePosX, {
      toValue: xPos,
      duration: 100,
    }).start(() => {
      Animated.sequence([
        Animated.timing(this.state.visible, {
          toValue: 1,
          duration: 2,
        }),
        Animated.timing(this.state._bottom, {
          toValue: this._height * 1.2,
          duration: 500,
        }),
        Animated.timing(this.state.visible, {
          toValue: 0.01,
          duration: 10,
        }),
        Animated.timing(this.state._bottom, {
          toValue: 60,
          duration: 100,
        }),
      ]).start(() => {
        this.setState({canFire: true});
      });
    });
  };
  startAnimation = tgtX => {
    this.setState({canFire: false});
    Animated.timing(this.state.rot, {
      toValue: tgtX,
      duration: 1000,
    }).start(() => {
      this.fireMissile(tgtX);
    });
  };
  state = {
    rot: new Animated.Value(this._width * 0.5),
    missilePosX: new Animated.Value(this._width * 0.6),
    _bottom: new Animated.Value(60),
    visible: new Animated.Value(0),
    enemyTop: new Animated.Value(-50),
    enemyLeft: new Animated.Value(0.01),
    canFire: true,
  };

  render() {
    return (
      <ImageBackground
        source={require('./game/space.jpg')}
        onTouchStart={e => {
          if (!this.state.canFire) {
            return;
          }
          const tgtX = e.nativeEvent.pageX;
          this.startAnimation(tgtX);
        }}
        style={{flex: 1}}>
        <StatusBar hidden={true} />

        <Animated.Image
          style={{
            flex: 1,
            position: 'absolute',
            top: this.state.enemyTop,
            left: this.state.enemyLeft,
            width: 50,
            height: 50,
	    transform:[{translateX:this.state.enemyTop,translateY:this.stateEnemyLeft}]
          }}
          resizeMode={'contain'}
          source={require('./game/ship.png')}
        />
        <Animated.View
          style={{
            flex: 1,
            opacity: this.state.visible,
            position: 'absolute',
            bottom: this.state._bottom,
            transform: [{translateX: this.state.missilePosX}],
          }}>
          <Image
            source={require('./game/Missile05.png')}
            style={{width: 50, height: 50}}
          />
        </Animated.View>
        <View style={{flex: 1}}>
          <Animated.Image
            style={{
              flex: 1,
              position: 'absolute',
              bottom: 10,
              width: 50,
              height: 50,
              transform: [{translateX: this.state.rot}],
            }}
            resizeMode={'contain'}
            source={require('./game/fighter.png')}
          />
        </View>
      </ImageBackground>
    );
  }
}

export default App;
