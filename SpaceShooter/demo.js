/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
    SafeAreaView,
    Animated,
    ScrollView,
    View,
    Text,
    Dimensions,
} from 'react-native';

class App extends React.Component {
    _width = Dimensions.get('window').widget;
    state = {
        rot: 0,
    };
    currentX = 0;
    currentY = 0;
    startAnimation = (rotx, roty) => {
        Animated.timing(this.state.rotationX, {
            toValue: rotx,
            duration: 100,
        }).start();
        Animated.timing(this.state.rotationY, {
            toValue: roty,
            duration: 100,
        }).start();
    };
    layoutGet = (event) => {
        //console.log(event.nativeEvent.layout.x)
        this.currentX = event.nativeEvent.layout.x;
        this.currentY = event.nativeEvent.layout.y;
    };

    render() {
        return (
            <View
                onTouchStart={e => {
                    let tgtX = e.nativeEvent.pageX;
                    /*let tgtY = this.currentY-e.nativeEvent.pageY;
                    const radius =Math.sqrt(tgtX*tgtX+tgtY*tgtY)
                    tgtX/=radius
                    tgtY/=radius
                    let rad = Math.atan2(tgtY, tgtX)*180 ;*/


                    this.setState({rot: tgtX});
                }}
                style={{flex: 1, backgroundColor: 'red'}}>

                <View style={{flex: 1}}>
                    <Animated.Image
                        onLayout={this.layoutGet}
                        style={{
                            flex: 1,
                            position: 'absolute',
                            bottom: 10,
                            width: 50,
                            height: 50,
                            transform: [{translateX: this.state.rot}],
                        }}
                        resizeMode={'contain'}
                        source={require('./game/shit.png')}
                    />
                </View>

            </View>
        );
    }
}

export default App;
