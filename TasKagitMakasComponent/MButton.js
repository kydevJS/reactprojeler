import React, { Component } from 'react'
import { Text,TouchableOpacity } from 'react-native'

export default class MButton extends Component {
   
   
    render() {
        const {metin,tiklandi}=this.props;
        return (
            <TouchableOpacity
      onPress={()=>tiklandi(metin)}
      style={{
        height: 50,
        width: 120,
        backgroundColor: 'pink',
        borderColor: 'purple',
        borderRadius: 25,
        borderWidth: 3,
        alignItems: 'center',
        justifyContent: 'center',
        
      }}>
      <Text style={{fontWeight: 'bold', fontSize: 18}}>{metin}</Text>
    </TouchableOpacity>
        )
    }
}
