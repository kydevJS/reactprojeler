import React from 'react';
import {
  View,Text, TouchableOpacity, ScrollView
} from 'react-native';

import {Button} from "./ExportTut/button";
import MClass,{Deneme,addItems} from "./ExportTut/MClass";
import MButton from "./TasKagitMakasComponent/MButton";
class App extends React.Component {
  
  state={game:[],canPlay:true,pcskor:0,adamskor:0}
 
  pcOyna=(adaminsecimi)=>
  { 
    const secenekler=["Rock","Paper","Scissors"]
    const secimIndex=Math.floor(Math.random()*3)
    const pcSecim=secenekler[secimIndex]
    const ai={kim:"Bilgisayar",secim:pcSecim}
    this.state.game.push(ai)
    const game=this.state.game
    this.setState({game,canPlay:true})
    if(adaminsecimi==pcSecim){}//beraber
    if((adaminsecimi=="Rock" && pcSecim=="Scissors")|| (adaminsecimi=="Paper" && pcSecim=="Rock") ||
    (adaminsecimi=="Scissors" && pcSecim=="Paper")){

        this.setState({adamskor:++this.state.adamskor})
    }else if((pcSecim=="Rock" && adaminsecimi=="Scissors")||
    (pcSecim=="Paper" && adaminsecimi=="Rock")||(pcSecim=="Scissors" && adaminsecimi=="Paper")){
      this.setState({pcskor:++this.state.pcskor})
    }

  }
  tiklandi=(metin)=>{ 
    if(this.state.canPlay==false) return;   
    const adamOynadi={kim:"sen",secim:metin}
    this.setState({game:[...this.state.game,adamOynadi]})
     /*this.state.game.push(adamOynadi);
    const game=this.state.game
   this.setState({
      game,canPlay:false
    })*/
    setTimeout(this.pcOyna,2000,metin)
  }





  render() {
    return (
      <View>
        <ScrollView style={{height:"60%"}}>

          {
           this.state.game.map(item=>{
           return (<Text key={item.kim}>{item.kim}---{item.secim}</Text>)
           })
          }

        </ScrollView>



        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 25,
            marginTop: 10,
            marginLeft: 10,
          }}>
          Pick One:{' '}
        </Text>
        <View
          style={{
            justifyContent: 'space-around',
            flexDirection: 'row',
            marginTop: 20,
          }}>
            <MButton metin={"Rock"} tiklandi={this.tiklandi}/>
            <MButton metin={"Paper"} tiklandi={this.tiklandi}/>
            <MButton metin={"Scissors"} tiklandi={this.tiklandi}/>
                 
        </View>
        <Text>Sen :{this.state.adamskor} PC :{this.state.pcskor}</Text>
      </View>
    );
  }

}
export default App;
