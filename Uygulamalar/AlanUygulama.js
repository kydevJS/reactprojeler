import React, { Component } from 'react';
import Login from "./src/Login";
import {TouchableOpacity,View,Dimensions,Text,CheckBox,Slider, TextInput} from "react-native";
import YuvarlakButton from "./src/YuvarlakButton";
import YuvarlakButtonNeg from "./src/YuvarlakButtonNeg";
import WeatherApp from "./src/WeatherApp";
import MButton from './PropMantigi/MButton';
import StateApp from './StateMantigi/StateApp';
import Page1 from './Pages/Page1';
import Page2 from './Pages/Page2';

class App extends Component {
 
state={yaricap:"",alan:0,cevre:0}

  render() {
    return (
      <View>
      <Text>Girilen {this.state.yaricap} yarıçaplı çemberin
      alanı : {this.state.alan} ve çevresi {this.state.cevre}
      
      </Text>
      <TextInput value={this.state.yaricap} 
      keyboardType="numeric"
      onChangeText={(text)=>{
       
          //const text=parseFloat(text);
          const alan=(Math.PI*text*text).toFixed(2);
          const cevre=(2*text*Math.PI).toFixed(2); 
          const yaricap=text;
          this.setState({alan,cevre,yaricap})          

      }}
      placeholder="Lütfen yarıçap uzunluğunu giriniz"/>
    </View>)
    
  }
}
export default App;
