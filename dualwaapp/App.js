/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Image,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Dimensions,
  FlatList,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

import {getLanguages} from 'react-native-i18n';
import RadioButton from './RadioButton';

class App extends React.Component {
  state = {lang: '', activeItem: 0};
  w = Dimensions.get('window').width;
  BASEURL = 'http://omboinc.site/dualwa/app/walk-subs-images/';
  constructor(props) {
    super(props);
    this.getSystemLang();
  }
  walks = [
    {
      id: '1',
      url: this.BASEURL + 'walk1.png',
      title: 'iPhone Wallpape',
      text: 'Great wallpaper for your phone...',
    },
    {
      id: '2',
      url: this.BASEURL + 'walk2.png',
      title: 'WA',
      text: 'Just like a web version of wa...',
    },
    {
      id: '3',
      url: this.BASEURL + 'walk3.png',
      title: 'Subscribe',
      text: 'Remove ads download new Wallpaper and access all features',
    },
  ];
  getSystemLang() {
    getLanguages().then((languages) => {
      this.setState({lang: languages[0]});
    });
  }
  onViewableItemsChanged = ({viewableItems}) => {
    //alert(JSON.stringify(viewableItems));
    const id = viewableItems[0].key;
    const index = this.walks.findIndex((item) => item.id == id);
    this.setState({activeItem: index});
  };
  renderItem(item) {
    return (
      <View style={{alignItems: 'center', width: this.w, height: 350}}>
        <Image
          source={{uri: item.url}}
          resizeMode={'contain'}
          style={{width: 400, height: 300}}
        />
        <Text style={{color: 'white', fontSize: 25, fontWeight: 'bold'}}>
          {item.title}
        </Text>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            marginHorizontal: 20,
            textAlign: 'center',
            marginTop: 10,
          }}>
          {item.text}
        </Text>
      </View>
    );
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#1c0b3c'}}>
        <StatusBar hidden={true} />

        <FlatList
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          pagingEnabled={true}
          horizontal={true}
          data={this.walks}
          onViewableItemsChanged={this.onViewableItemsChanged}
          renderItem={({item}) => this.renderItem(item)}
          keyExtractor={(item) => item.id}
        />
        {this.state.activeItem == 2 && (
          <View style={{position: 'absolute', right: 25, top: 10}}>
            <TouchableOpacity>
              <Image
                source={require('./images/iconClose.png')}
                style={{width: 30, height: 30}}
              />
            </TouchableOpacity>
          </View>
        )}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            flex: 1,
            marginTop: 20,
          }}>
          <RadioButton
            isActive={this.state.activeItem == 0 ? true : false}
            size={30}
            bsize={10}
          />
          <RadioButton
            isActive={this.state.activeItem == 1 ? true : false}
            size={30}
            bsize={10}
          />
          <RadioButton
            isActive={this.state.activeItem == 2 ? true : false}
            size={30}
            bsize={10}
          />
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={{color: 'white'}}>Weekly is 5.99$/3 Day free</Text>
        </View>
        <View style={{flex: 1, alignItems: 'center'}}>
          <TouchableOpacity
            style={{
              backgroundColor: 'green',
              height: 56,
              width: 252,
              justifyContent: 'center',
              borderColor: 'green',
              alignItems: 'center',
              borderRadius: 5,
              borderWidth: 1,
            }}>
            <Text style={{color: 'white'}}>
              {this.state.activeItem == 2 ? 'Start' : 'Continue'}
            </Text>
          </TouchableOpacity>
        </View>
        {this.state.activeItem == 2 && (
          <View style={{flex: 1, alignItems: 'center'}}>
            <TouchableOpacity>
              <Text style={{color: 'grey', fontSize: 15}}>
                Restore Purchase
              </Text>
            </TouchableOpacity>
          </View>
        )}

        {this.state.activeItem == 2 && (
          <View style={{flex: 1, alignItems: 'center', marginHorizontal: 20}}>
            <ScrollView>
              <Text style={{color: 'grey', fontSize: 15, textAlign: 'center'}}>
                Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır
                metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir
                hurufat numune kitabı oluşturmak üzere bir yazı galerisini
                alarak karıştırdığı 1500'lerden beri endüstri standardı sahte
                metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını
                sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik
                dizgiye de sıçramıştır. 1960'larda Lorem Ipsum pasajları da
                içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda
                Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü
                yayıncılık yazılımları ile popüler olmuştur.
              </Text>
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({});

export default App;
