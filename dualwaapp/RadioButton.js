import React from 'react';
import {TouchableOpacity, View} from 'react-native';

function RadioButton({isActive, size, bsize}) {
  return (
    <TouchableOpacity
      style={{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderColor: 'white',
        width: size,
        height: size,
        margin: 5,
        borderRadius: size / 2,
      }}>
      {isActive && (
        <View
          style={{
            backgroundColor: 'green',
            borderColor: 'green',
            width: bsize,
            height: bsize,
            borderRadius: bsize / 2,
          }}
        />
      )}
    </TouchableOpacity>
  );
}
export default RadioButton;
