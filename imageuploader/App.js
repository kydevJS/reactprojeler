/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Button,
  View,
  Image,
  StatusBar,
} from 'react-native';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
  title: 'Select Avatar',
  customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

/**
 * The first arg is the options object for customization (it can also be null or omitted for default options),
 * The second arg is the callback which sends object: response (more info in the API Reference)
 */

class App extends React.Component {
  state = {avatarSource: null};

  upload = response => {
    const form = new FormData();
    form.append('img', {
      uri: response.uri,
      type: response.type,
      name: response.fileName,
    });
    form.append('extra', 'yahya');

    const config = {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
    };
    axios
      .post('http://www.sharer.tk/upload_image.php', form, config)
      .then(response => alert(response.data))
      .catch(error => alert(error.message));
  };
  pick = () => {
    ImagePicker.showImagePicker(options, response => {
      //console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        this.upload(response);

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
        });
      }
    });
  };

  render() {
    return (
      <View style={{alignItems: 'center', marginTop: 50}}>
        <View
          style={{
            width: 200,
            height: 200,
            borderWidth: 1,
            borderRadius: 2,
            borderStyle: 'dashed',
            borderColor: 'gray',
          }}>
          {this.state.avatarSource && (
            <Image
              source={this.state.avatarSource}
              style={{
                width: 200,
                height: 200,
              }}
            />
          )}
        </View>

        <View style={{marginTop: 20}}>
          <Button onPress={this.pick} title={'select a photo'} />
        </View>
      </View>
    );
  }
}

export default App;
