/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Loading from "./Loading"
import Register from "./Register"
import React from 'react';
import {
    StyleSheet,
    Button,
    View, TouchableOpacity,
    Text,
    StatusBar,
} from 'react-native';
const Stack = createStackNavigator();
class App extends React.Component {

render()
{
    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{header:()=>null}}>
                <Stack.Screen name="home" component={Loading} />
                <Stack.Screen name="register" component={Register} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

}

const styles = StyleSheet.create({});

export default App;
