/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet,
    Button,
    View, TouchableOpacity,
    Text,
    StatusBar,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {observer} from "mobx-react"
import {observable} from "mobx";

@observer
class Loading extends React.Component {
    @observable count = 1
    @observable metin = ""
    save = async (txt) => {

        const
            saver = await AsyncStorage.setItem("@deneme", txt)
        alert("Başarıyla yazıldı! Okumak için read e basınız...")


    }

    read = async () => {
        var val = await AsyncStorage.getItem("@deneme");
        if (val == "" || val == null)
            val = "Herhangi bir veri bulunmuyor."
        this.metin = val


    }
    del = async () => {
        await AsyncStorage.setItem("@deneme", "");
        this.metin = "Herhangi bir veri bulunmuyor."
    }

    componentDidMount() {
        this.read()
    }

    render() {
        return (
            <View style={{flex: 1, justifyContent: "center", marginTop: 10, marginRight: 20, marginLeft: 20}}>
                <StatusBar hidden={true}/>
                <Text style={{textAlign: "center"}}>{this.count}</Text>
                <View style={{alignItems: "center",}}>
                    <TouchableOpacity style={{
                        marginTop: 20,
                        marginBottom: 20,
                        backgroundColor: "yellow",
                        borderWidth: 1,
                        borderRadius: 10,
                        width: "60%"
                    }} onPress={() => this.count++}>
                        <Text style={{textAlign: "center"}}>{"Sayaç artır"}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{height: 50}}>
                    <Text>{"Storage Metni => "}{this.metin}</Text>
                </View>
                <View>
                    <Button title={"Oku"} onPress={() => this.read()}/>
                </View>
                <View style={{height: 10}}></View>
                <View>
                    <Button title={"Yaz"} onPress={() => this.save("işte bunu yazmıştık!")}/>
                </View>

                <View style={{height: 10}}></View>
                <View>
                    <Button title={"Sil"} onPress={() => this.del()}/>
                </View>

                <View style={{height: 10}}></View>
                <View>
                    <Button title={"Diğer Sayfaya Geç"} onPress={() => this.props.navigation.navigate("register")}/>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({});

export default Loading;
