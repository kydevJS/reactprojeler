/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
    StyleSheet,
    Button,
    View,
    StatusBar,
} from 'react-native';


class Register extends React.Component {
    render() {
        return (
            <View style={{flex: 1, justifyContent: "center", marginTop: 10, marginRight: 20, marginLeft: 20}}>
                <StatusBar hidden={true}/>
                <View>
                    <Button title={"Geri Git <<"} onPress={() => this.props.navigation.pop()}/>
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({});

export default Register;
