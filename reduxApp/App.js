/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux';
import store from "./src/store/index";
import AddTodo from './src/components/AddTodo';
import ListTodos from './src/components/ListTodos';
/*
import SayHello from './src/MFuncModule';
const App= () => {
  return (
    <View>  
    <MButton text={"text geldi"} name={"name geldi"} sayi={10}/>
    <SayHello text={"Merhabs"}/> 
    </View>
  );
};
function MButton({text,name,sayi}){
  const _sayi=sayi*10;
  return(<View><Text>{text} {name} {_sayi}</Text></View>)
}*/

const App=() => {
  return (
    <Provider store={store}>  
    <AddTodo dispatch={store.dispatch}/>
    <ListTodos/>
    </Provider>
  );
};



export default App;
