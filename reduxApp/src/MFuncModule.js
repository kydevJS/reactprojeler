import React from "react";
import {TouchableHighlight,View,Text} from "react-native"
 SayHello=({text})=>{
    const [title,setTitle]=React.useState(text);

    return (<TouchableHighlight
    onPress={()=>setTitle("Merhabalar")}
    >
        <View>
            <Text>{title}</Text>
        </View>
    </TouchableHighlight>)
}
export default SayHello;