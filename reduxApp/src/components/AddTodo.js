import React from "react"
import { View, TextInput, TouchableOpacity,Text } from "react-native"
const AddTodo = ({dispatch})=>{
    const [text,setText]=React.useState("")
    return(
<View style={{flexDirection:"row",margin:20,justifyContent:"center",alignItems:"center"}}>
    <TextInput
    value={text}
    onChangeText={(title)=>setText(title)}
    placeholder={"Bir not ekle"} style={{borderWidth:0.3,width:"80%",height:50}} />
    <TouchableOpacity style={{marginLeft:10}} onPress={()=>{
        dispatch({type:"ADD_TODO",text:text})
        setText("")
        }        
        }>
        <Text>Not ekle</Text>
    </TouchableOpacity>
</View>

)
}

export default AddTodo