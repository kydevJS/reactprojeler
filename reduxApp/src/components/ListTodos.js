import React from "react"
import { View, TouchableOpacity,Text } from "react-native"
import {connect} from "react-redux"
const ListTodos=({todos,toggleTodo}) =>{

    return (
        <View>
            {todos.map(todo=>{
                return (<TouchableOpacity>
                    <Text>{todo.text}</Text>
                </TouchableOpacity>)

            })}
        </View>

    )
}

const dispatchToProps=dispatch=>({
    toggleTodo:(id)=>dispatch({type:"TOGGLE_TODO",id:id}),
})

const propsToState=(state)=>(
    {todos:state.todos}
)


//ilk fonksiyon her zaman state, diğerleri de artık ne ise o
//state gerek yoksa null geçilebilir
export default connect(propsToState,dispatchToProps)(ListTodos)