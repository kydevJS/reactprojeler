import visibilitifilter from "./visibilitifilter"
import todos from "./todos"
import {combineReducers} from "redux"

const rootReducer=combineReducers({visibilitifilter,todos})
export default rootReducer