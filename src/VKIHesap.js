import React, {Component} from 'react';
import YuvarlakButton from "./YuvarlakButton";
import YuvarlakButtonNeg from "./YuvarlakButtonNeg";
import {TouchableOpacity, Text, View, CheckBox, Slider} from 'react-native';

export default class VKIHesap extends Component {
  render() {
    return (
      <View>
        <View>
          <Text style={{textAlign: 'center'}}>KVI Hesaplama</Text>
        </View>
        <View style={{
            marginTop:30,
            flexDirection:"row",
            justifyContent:"space-evenly"
        }}>
            <Text>Erkek</Text>
            <CheckBox value={true}></CheckBox>
            <Text>Kadın</Text>
            <CheckBox></CheckBox>
        </View>
        <Text style={{textAlign: 'center',marginTop:20}}>Ağırlık</Text>
        <Text style={{textAlign: 'center',marginTop:10,color:"orange",fontSize:30,fontWeight:"bold"}}>60kg</Text>
        <View style={{
            marginTop:30
        }}>
            <Slider value={60} minimumValue={50} maximumValue={100}></Slider>
        </View>
        <View style={{
            marginTop:30,
            flexDirection:"row",
            justifyContent:"space-evenly"
        }}>
        <Text style={{textAlign: 'center',marginTop:20}}>Yaş</Text>
        <Text style={{textAlign: 'center',marginTop:20}}>Boy</Text>
        </View>
        <View style={{
            flexDirection:"row",
            justifyContent:"space-evenly"
        }}>
        <Text style={{textAlign: 'center',color:"orange",fontSize:30,fontWeight:"bold"}}>20</Text>
        <Text style={{textAlign: 'center',color:"orange",fontSize:30,fontWeight:"bold"}}>20</Text>
        </View>
        <View style={{
            marginTop:30,
            flexDirection:"row",
            justifyContent:"space-around"
        }}>
        <Text style={{textAlign: 'center',marginTop:20}}>Yaş</Text>
        <Text style={{textAlign: 'center',marginTop:20}}>Boy</Text>
        </View>
        <View style={{
            marginTop:30,
            flexDirection:"row",
            justifyContent:"space-evenly"
        }}>
         <YuvarlakButtonNeg/>
         <YuvarlakButton/>
         <YuvarlakButtonNeg/>
         <YuvarlakButton/>
        </View>
        <TouchableOpacity style={{
            backgroundColor:"red",
            height:30,
            width:"100%",
            position:"absolute",
            justifyContent:630
            

        }}>
            <Text style={{textAlign:"center"}}>Hesapla</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
