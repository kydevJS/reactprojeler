import React, { Component } from 'react'
import { Text, View,StatusBar,
    StyleSheet,
    Dimensions,
    TextInput,
    Image } from 'react-native'

import axios from "axios";

export default class WeatherApp extends Component {
    apiKey="http://api.openweathermap.org/data/2.5/weather?units=imperial&appid=1df40a009087ebb29c50af89e36d5989&q=";
    state={main:"",description:"",temp:""}

    setWeather=async (text)=>{
        var url = this.apiKey+text
       await axios.get(url).then(response=>{
        var gelenVeri=response.data;
        var main=gelenVeri.weather[0].main
        var description=gelenVeri.weather[0].description;
        var temp=gelenVeri.main.temp;
        this.setState({main:main,description:description,temp:temp})
       
       }).catch(eror=>{
           console.log(eror)
       })
    
    }

    render() {
      
        const width=Dimensions.get("window").width;
        const height=Dimensions.get("window").height;
        return (

            <View>
                <StatusBar hidden={true}/>
                <Image style={{position:"absolute",
                resizeMode:"stretch",
                width:width,
                height:height}} source={require("../images/bg.jpg")}/>
                <View style={{height:height*0.3,
                    opacity:0.3,
                    backgroundColor:"black"}}>

                        <View style={{flexDirection:"row",
                        marginTop:10,
                        alignItems:"center",justifyContent:"space-around"
                    }}>
                            <Text style={{color:"white"}}>Current weather for</Text>
                            <TextInput
                            onChangeText={this.setWeather}
                            style={{
                            color:"white",
                            fontSize:16,
                            width:100,
                            height:40,
                            borderColor:"white",
                            borderBottomWidth:1,
                        }}/>
                        </View>
                        <View style={{alignItems:"center",marginTop:30,}}>
                            <Text style={styles.textStil}>{this.state.main}</Text>
                        </View>
                        <View style={{flexDirection:"row",justifyContent:"center",marginTop:25,}}>
                            <Text  style={styles.textStilNormal}>Current Weather Condition :</Text>
                            <Text  style={styles.textStilNormal}>{this.state.description}</Text>
                        </View>
                        <View style={{alignItems:"center",marginTop:25,}}>
                            <Text style={[styles.textStilNormal,{fontWeight:"bold",fontSize:20}]}>{this.state.temp} °F</Text>
                        </View>


                </View>
                
            </View>
        )
    }
}
const styles=StyleSheet.create({
    textStil:{
        color:"white",
        fontSize:18,
        fontWeight:"bold"
    }, 
    textStilNormal:{
        color:"white",
        fontSize:18
    }

});