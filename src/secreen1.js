import React, { Component } from 'react';
import {
  Image,
  TouchableOpacity,
  View,
  Text
} from 'react-native';
import {styles} from "../stil";
class Screen1 extends Component{
    render() {

        return (
          <View>
    
            <View>
              <Text style={styles.baslik}>Trip to Sydney</Text>
            </View>
    
            <View>
              <TouchableOpacity>
                <Text style={styles.buton}>Share with onyone</Text>
              </TouchableOpacity>
            </View>
    
            <View>
              <TouchableOpacity>
                <Text style={styles.buton}>Share in field trippa</Text>
              </TouchableOpacity>
            </View>
    
            <View>
              <TouchableOpacity style={styles.buton2}>
                <Text style={styles.buton2Yazi}>+ Add photo</Text>
              </TouchableOpacity>
            </View>
    
            <View>
                <Image
                  source={require('../images/sydney.jpg')}
                  style={{width: 200, height: 200, marginHorizontal: 30, marginTop: 10}} 
                />
            </View>
            
          </View>
       
        );
      }
}


  export default Screen1;