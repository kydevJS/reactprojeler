import React, { Component } from 'react';
import {StyleSheet} from "react-native";
export  const styles = StyleSheet.create({
    baslik:{
      fontSize: 20,
      marginTop: 15,
      marginLeft: 15
    },
    buton: {
      textAlign: 'center',
      marginTop: 20,
      color: 'green',
      fontSize:14
    },
    buton2:{
      height: 30,
      width: 200,
      backgroundColor: 'green',
      marginHorizontal: 40,
      marginTop: 10,
      borderRadius: 8
    },
    buton2Yazi:{
      textAlign: 'center',
      marginTop: 5,
      color: 'white',
    
    }
  })